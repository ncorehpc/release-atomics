/********************************************************************
 * BenchIT - Performance Measurement for Scientific Applications
 * Contact: developer@benchit.org
 *
 * $Id: BIEnvHash.template.java 1 2009-09-11 12:26:19Z william $
 * $URL: svn+ssh://benchit@rupert.zih.tu-dresden.de/svn-base/benchit-root/BenchITv6/tools/BIEnvHash.template.java $
 * For license details see COPYING in the package base directory
 *******************************************************************/
import java.util.HashMap;
import java.util.Iterator;
import java.io.*;

public class BIEnvHash
{
   private static BIEnvHash instance = null;
   private HashMap table = null;
   private BIEnvHash()
   {
      bi_initTable();
      bi_fillTable();
   }
   public static BIEnvHash getInstance()
   {
      if ( instance == null ) instance = new BIEnvHash();
      return instance;
   }
   /** Puts a new key value mapping into the map and returns the
     * previously assigned value to the key, or null, if it's a
     * new mapping. */
   public String bi_setEnv( String key, String value )
   {
      return bi_put( key, value );
   }
   /** Puts a new key value mapping into the map and returns the
     * previously assigned value to the key, or null, if it's a
     * new mapping. */
   public String bi_put( String key, String value )
   {
      if ( table == null ) bi_initTable();
      Object ret = table.put( key, value );
      if ( ret == null ) return null;
      return (String)ret;
   }
   public String bi_getEnv( String key )
   {
      if (System.getenv(key)!=null) return System.getenv(key);
      return bi_get( key );
   }
   public String bi_get( String key )
   {
      if ( table == null ) return null;
      return (String)(table.get( key ));
   }
   public void bi_dumpTable()
   {
      if ( table == null ) return;
      printf( "\nHashtable dump of all known environment variables at compiletime:" );
      printf( "\n Key            | Value" );
      printf( "\n----------------------------------" );
      Iterator it = table.keySet().iterator();
      while ( it.hasNext() )
      {
         String key = (String)(it.next());
         String value = (String)(table.get( key ));
         printf( "\n " + key + " | " + value );
      }
      printf( "\n" + bi_size() + " entries in total.\n" );
      flush();
   }
   public void dumpToOutputBuffer( StringBuffer outputBuffer )
   {
      outputBuffer.append( "beginofenvironmentvariables\n" );
      Iterator it = table.keySet().iterator();
      while ( it.hasNext() )
      {
         String key = (String)(it.next());
         String value = (String)(table.get( key ));
         StringBuffer b = new StringBuffer();
         for ( int i = 0; i < value.length(); i++ )
         {
            if ( value.charAt( i ) == '"' ) {
               /* check equal number of escape chars->insert escape char */
               int cnt = 0;
               for ( int j = i - 1;  j >= 0; j-- ) {
                  if ( value.charAt( j ) == '\\' ) cnt++;
               }
               /* escape quote */
               if ( cnt % 2 == 0 ) {
                  b.append( '\\' );
               }
            }
            b.append( value.charAt( i ) );
         }
         value = b.toString();
         outputBuffer.append( key + "=\"" + value + "\"\n" );
      }
      outputBuffer.append( "endofenvironmentvariables\n" );
   }
   public int bi_size()
   {
      if ( table != null ) return table.size();
      return -1;
   }
   public void bi_initTable()
   {
      table = new HashMap( 1009 );
   }
   private void printf( String str )
   {
      System.out.print( str );
   }
   private void flush()
   {
      System.out.flush();
      System.err.flush();
   }
   /** Takes a PARAMETER file as argument and adds it's variables
     * to the environment HashMap. */
   public boolean bi_readParameterFile( String fileName )
   {
      boolean retval = false;
      File textFile = null;
      try {
         textFile = new File( fileName );
      }
      catch ( NullPointerException npe ) {
         return retval;
      }
      if ( textFile != null ) {
         if ( textFile.exists() ) {
            FileReader in;
            char ch = '\0';
            int size = (int)(textFile.length());
            int read = 0;
            char inData[] = new char[size + 1];
            try {
               in = new FileReader( textFile );
               in.read( inData, 0, size );
               in.close();
            }
            catch( IOException ex ) {
               return retval;
            } // scan inData for environment variables
            int scanned = 0;
            int it = 0;
            ch = inData[scanned];
            StringBuffer buffer = null;
            while ( scanned < size ) {
               buffer = new StringBuffer();
               // read a line
               while ( ch != '\n' ) {
                  if ( ch != '\r' ) {
                     buffer.append( ch );
                  }
                  scanned += 1;
                  ch = inData[scanned];
               }
               // trim leading and trailing white spaces
               String lineValue = buffer.toString().trim();
               // now check the line
               int eqIndex = -1;
               try {
                  eqIndex = lineValue.indexOf( "=" );
               }
               catch ( NullPointerException npe ) {
                  return retval;
               }
               // only check lines containing an equals sign, no $ sign,
               // and the start with a capital letter
               if ( eqIndex >= 0 && lineValue.indexOf( "$" ) < 0 &&
                  lineValue.substring( 0, 1 ).matches( "[A-Z]" ) ) {
                  String varname = null;
                  String varvalue = null;
                  try {
                     varname = lineValue.substring( 0 , eqIndex );
                     varvalue = lineValue.substring( eqIndex + 1 );
                  }
                  catch ( IndexOutOfBoundsException ioobe ) {
                  }
                  if ( ( varname != null ) && ( varvalue != null ) ) {
                     boolean st1=false, en1=false, st2=false, en2=false;
                     boolean st3=false, en3=false;
                     try {
                        st1=varvalue.startsWith( "'" );
                        en1=varvalue.endsWith( "'" );
                        st2=varvalue.startsWith( "(" );
                        en2=varvalue.endsWith( ")" );
                        st3=varvalue.startsWith( "\"" );
                        en3=varvalue.endsWith( "\"" );
                     }
                     catch ( NullPointerException npe ) {
                        return retval;
                     }
                     if ( ( ( st1 == en1 ) && ( ( st1 != st2 ) && ( st1!=st3 ) ) ) ||
                          ( ( st2 == en2 ) && ( ( st2 != st1 ) && ( st2!=st3 ) ) ) ||
                          ( ( st3 == en3 ) && ( ( st3 != st1 ) && ( st3!=st2 ) ) ) ||
                          ( ( st1 == en1 ) && ( st2 == en2 ) && ( st3 == en3 )
                          && ( st1 == st2 ) && ( st2 == st3 ) && ( st3 == false ) ) ) {
                        table.put( varname, varvalue );
                     }
                  }
               }
               scanned += 1;
               ch = inData[scanned];
               it++;
            }
         }
         else {
            System.err.println( "BenchIT: PARAMETER file \"" + fileName
               + "\" doesn't exist." );
            return retval;
         }
      }
      else {
         System.err.println( "BenchIT: PARAMETER file \"" + fileName
               + "\" not found." );
         return retval;
      }
      return true;
   }
   /* special case: generated from outside and code will be
      appended to this file */
   /** Fills the table with predefined content. */
   public void bi_fillTable()
   {

      bi_put( "BENCHITROOT", "/home/thea/build/release-atomics" );
      bi_put( "BENCHIT_ARCH_SHORT", "unknown" );
      bi_put( "BENCHIT_ARCH_SPEED", "unknownM" );
      bi_put( "BENCHIT_CC", "riscv64-unknown-linux-gnu-clang" );
      bi_put( "BENCHIT_CC_C_FLAGS", "" );
      bi_put( "BENCHIT_CC_C_FLAGS_HIGH", "-O0" );
      bi_put( "BENCHIT_CC_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_CC_C_FLAGS_STD", "-O2" );
      bi_put( "BENCHIT_CC_LD", "riscv64-unknown-linux-gnu-clang" );
      bi_put( "BENCHIT_CC_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_COMMENT", "memory read latency" );
      bi_put( "BENCHIT_CPP_ACML", "" );
      bi_put( "BENCHIT_CPP_ATLAS", "" );
      bi_put( "BENCHIT_CPP_BLAS", "" );
      bi_put( "BENCHIT_CPP_ESSL", "" );
      bi_put( "BENCHIT_CPP_FFTW3", "" );
      bi_put( "BENCHIT_CPP_MKL", "" );
      bi_put( "BENCHIT_CPP_MPI", " -DUSE_MPI" );
      bi_put( "BENCHIT_CPP_PAPI", "-DUSE_PAPI" );
      bi_put( "BENCHIT_CPP_PCL", " -DUSE_PCL" );
      bi_put( "BENCHIT_CPP_PTHREADS", "" );
      bi_put( "BENCHIT_CPP_PVM", "" );
      bi_put( "BENCHIT_CPP_SCSL", "" );
      bi_put( "BENCHIT_CROSSCOMPILE", "0" );
      bi_put( "BENCHIT_CXX", "riscv64-unknown-linux-gnu-clang++" );
      bi_put( "BENCHIT_CXX_C_FLAGS", "" );
      bi_put( "BENCHIT_CXX_C_FLAGS_HIGH", "-O3" );
      bi_put( "BENCHIT_CXX_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_CXX_C_FLAGS_STD", "-O2" );
      bi_put( "BENCHIT_CXX_LD", "riscv64-unknown-linux-gnu-clang++" );
      bi_put( "BENCHIT_CXX_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_DEBUGLEVEL", "0" );
      bi_put( "BENCHIT_DEFINES", " -DDEBUGLEVEL=0" );
      bi_put( "BENCHIT_ENVIRONMENT", "DEFAULT" );
      bi_put( "BENCHIT_F77", "" );
      bi_put( "BENCHIT_F77_C_FLAGS", "" );
      bi_put( "BENCHIT_F77_C_FLAGS_HIGH", "" );
      bi_put( "BENCHIT_F77_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_F77_C_FLAGS_STD", "" );
      bi_put( "BENCHIT_F77_LD", "" );
      bi_put( "BENCHIT_F77_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_F90", "" );
      bi_put( "BENCHIT_F90_C_FLAGS", "" );
      bi_put( "BENCHIT_F90_C_FLAGS_HIGH", "" );
      bi_put( "BENCHIT_F90_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_F90_C_FLAGS_STD", "" );
      bi_put( "BENCHIT_F90_LD", "" );
      bi_put( "BENCHIT_F90_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_F90_SOURCE_FORMAT_FLAG", "" );
      bi_put( "BENCHIT_F95", "" );
      bi_put( "BENCHIT_F95_C_FLAGS", "" );
      bi_put( "BENCHIT_F95_C_FLAGS_HIGH", "" );
      bi_put( "BENCHIT_F95_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_F95_C_FLAGS_STD", "" );
      bi_put( "BENCHIT_F95_LD", "" );
      bi_put( "BENCHIT_F95_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_F95_SOURCE_FORMAT_FLAG", "" );
      bi_put( "BENCHIT_FILENAME_COMMENT", "0" );
      bi_put( "BENCHIT_HOSTNAME", "hokuto.kashira.local" );
      bi_put( "BENCHIT_IGNORE_PARAMETER_FILE", "0" );
      bi_put( "BENCHIT_INCLUDES", "-I. -I/home/thea/build/release-atomics -I/home/thea/build/release-atomics/tools" );
      bi_put( "BENCHIT_INTERACTIVE", "0" );
      bi_put( "BENCHIT_JAVA", "java" );
      bi_put( "BENCHIT_JAVAC", "" );
      bi_put( "BENCHIT_JAVAC_FLAGS", "" );
      bi_put( "BENCHIT_JAVAC_FLAGS_HIGH", "" );
      bi_put( "BENCHIT_JAVA_FLAGS", "" );
      bi_put( "BENCHIT_JAVA_HOME", "" );
      bi_put( "BENCHIT_KERNELBINARY", "/home/thea/build/release-atomics/bin/arch_riscv64.memory_latency.C.pthread.0.cmpxchg128-succ.0" );
      bi_put( "BENCHIT_KERNELNAME", "arch_riscv64.memory_latency.C.pthread.0.cmpxchg128-succ" );
      bi_put( "BENCHIT_KERNEL_ACCESSES", "256" );
      bi_put( "BENCHIT_KERNEL_ALIGNMENT", "256" );
      bi_put( "BENCHIT_KERNEL_ALLOC", "L" );
      bi_put( "BENCHIT_KERNEL_COMMENT", " 0B missaligned, alloc: L, hugep.: 0, use: F4, flush: 111 - E2, TLB: 0" );
      bi_put( "BENCHIT_KERNEL_COUNTERS", "PAPI_L2_DCM" );
      bi_put( "BENCHIT_KERNEL_CPU_LIST", "0,2" );
      bi_put( "BENCHIT_KERNEL_ENABLE_PAPI", "0" );
      bi_put( "BENCHIT_KERNEL_FLUSH_ACCESSES", "2" );
      bi_put( "BENCHIT_KERNEL_FLUSH_L1", "1" );
      bi_put( "BENCHIT_KERNEL_FLUSH_L2", "1" );
      bi_put( "BENCHIT_KERNEL_FLUSH_L3", "1" );
      bi_put( "BENCHIT_KERNEL_FLUSH_MODE", "E" );
      bi_put( "BENCHIT_KERNEL_HUGEPAGES", "0" );
      bi_put( "BENCHIT_KERNEL_HUGEPAGE_DIR", "/mnt/huge" );
      bi_put( "BENCHIT_KERNEL_MAX", "200000000" );
      bi_put( "BENCHIT_KERNEL_MIN", "4000" );
      bi_put( "BENCHIT_KERNEL_OFFSET", "0" );
      bi_put( "BENCHIT_KERNEL_RUNS", "4" );
      bi_put( "BENCHIT_KERNEL_SERIALIZATION", "mfence" );
      bi_put( "BENCHIT_KERNEL_SHARE_CPU", "3" );
      bi_put( "BENCHIT_KERNEL_STEPS", "100" );
      bi_put( "BENCHIT_KERNEL_TIMEOUT", "3600" );
      bi_put( "BENCHIT_KERNEL_TLB_MODE", "0" );
      bi_put( "BENCHIT_KERNEL_USE_ACCESSES", "4" );
      bi_put( "BENCHIT_KERNEL_USE_MODE", "F" );
      bi_put( "BENCHIT_LD_LIBRARY_PATH", "/home/thea/build/release-atomics/jbi/jni" );
      bi_put( "BENCHIT_LIB_ACML", " -lacml" );
      bi_put( "BENCHIT_LIB_ATLAS", " -latlas" );
      bi_put( "BENCHIT_LIB_BLAS", "-lblas" );
      bi_put( "BENCHIT_LIB_ESSL", " -lessl" );
      bi_put( "BENCHIT_LIB_FFTW3", " -lfftw3" );
      bi_put( "BENCHIT_LIB_MKL", " -lmkl" );
      bi_put( "BENCHIT_LIB_MPI", "" );
      bi_put( "BENCHIT_LIB_PAPI", "" );
      bi_put( "BENCHIT_LIB_PCL", "" );
      bi_put( "BENCHIT_LIB_PTHREAD", "" );
      bi_put( "BENCHIT_LIB_PVM", "" );
      bi_put( "BENCHIT_LIB_SCSL", " -lscsl" );
      bi_put( "BENCHIT_LOCAL_CC", "gcc" );
      bi_put( "BENCHIT_LOCAL_CC_C_FLAGS", "-O0" );
      bi_put( "BENCHIT_LOCAL_CC_L_FLAGS", "-lm" );
      bi_put( "BENCHIT_MANDATORY_FILES", "benchit.c interface.h tools/envhashbuilder.c tools/bienvhash.template.c tools/bienvhash.h tools/stringlib.c tools/stringlib.h " );
      bi_put( "BENCHIT_MPICC", "riscv64-unknown-linux-gnu-clang" );
      bi_put( "BENCHIT_MPICC_C_FLAGS", "" );
      bi_put( "BENCHIT_MPICC_C_FLAGS_HIGH", "-O3" );
      bi_put( "BENCHIT_MPICC_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_MPICC_C_FLAGS_STD", "-O2" );
      bi_put( "BENCHIT_MPICC_LD", "riscv64-unknown-linux-gnu-clang" );
      bi_put( "BENCHIT_MPICC_L_FLAGS", "-lm -lmpi" );
      bi_put( "BENCHIT_MPIF77", "" );
      bi_put( "BENCHIT_MPIF77_C_FLAGS", "" );
      bi_put( "BENCHIT_MPIF77_C_FLAGS_HIGH", "" );
      bi_put( "BENCHIT_MPIF77_C_FLAGS_OMP", "" );
      bi_put( "BENCHIT_MPIF77_C_FLAGS_STD", "" );
      bi_put( "BENCHIT_MPIF77_LD", "" );
      bi_put( "BENCHIT_MPIF77_L_FLAGS", "" );
      bi_put( "BENCHIT_MPIRUN", "mpirun" );
      bi_put( "BENCHIT_NODENAME", "hokuto" );
      bi_put( "BENCHIT_NUM_CPUS", "1" );
      bi_put( "BENCHIT_NUM_PROCESSES", "" );
      bi_put( "BENCHIT_NUM_THREADS_PER_PROCESS", "" );
      bi_put( "BENCHIT_OPTIONAL_FILES", "LOCALDEFS/PROTOTYPE_input_architecture LOCALDEFS/PROTOTYPE_input_display " );
      bi_put( "BENCHIT_PARAMETER_FILE", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ/PARAMETERS" );
      bi_put( "BENCHIT_PROGRESS_DIR", "progress" );
      bi_put( "BENCHIT_RUN_ACCURACY", "2" );
      bi_put( "BENCHIT_RUN_COREDUMPLIMIT", "0" );
      bi_put( "BENCHIT_RUN_EMAIL_ADDRESS", "" );
      bi_put( "BENCHIT_RUN_LINEAR", "0" );
      bi_put( "BENCHIT_RUN_MAX_MEMORY", "0" );
      bi_put( "BENCHIT_RUN_OUTPUT_DIR", "/home/thea/build/release-atomics/output" );
      bi_put( "BENCHIT_RUN_QUEUENAME", "" );
      bi_put( "BENCHIT_RUN_REDIRECT_CONSOLE", "" );
      bi_put( "BENCHIT_RUN_TEST", "0" );
      bi_put( "BENCHIT_RUN_TIMELIMIT", "3600" );
      bi_put( "BENCHIT_USE_VAMPIR_TRACE", "0" );
      bi_put( "BR", "7" );
      bi_put( "CHROME_CONFIG_HOME", "/home/thea/.config/" );
      bi_put( "CLUTTER_IM_MODULE", "fcitx" );
      bi_put( "CLUTTER_VBLANK", "none" );
      bi_put( "COLORTERM", "truecolor" );
      bi_put( "COLUMNS", "116" );
      bi_put( "COMMENT", "" );
      bi_put( "COMPILERNAME", "composer_xe_2011_sp1.11.344" );
      bi_put( "CONFIGURE_MODE", "COMPILE" );
      bi_put( "CPATH", "/home/thea/.local/include:/home/thea/.local/include:" );
      bi_put( "CUDA_HOME", "/usr/local/cuda-7.0" );
      bi_put( "CURDIR", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ" );
      bi_put( "CVS_RSH", "ssh" );
      bi_put( "DBUS_SESSION_BUS_ADDRESS", "unix:abstract=/tmp/dbus-nEnB8lTnz9,guid=44b6f0a40e792d60b3933c7a62ca6737" );
      bi_put( "DESKTOP_SESSION", "lightdm-xsession" );
      bi_put( "DESKTOP_STARTUP_ID", "i3/|home|thea|.local|bin|terminal/2527-7-hokuto_TIME136255" );
      bi_put( "DISPLAY", ":0" );
      bi_put( "EDITOR", "/usr/bin/vim" );
      bi_put( "FZF_COMPLETION_OPTS", "--border --info=inline" );
      bi_put( "FZF_DEFAULT_COMMAND", "fd --type file --follow --hidden --color=always" );
      bi_put( "FZF_DEFAULT_OPTS", "-e --height 40% --layout=reverse --border --ansi" );
      bi_put( "GDFONTPATH", "/usr/share/fonts/bitstream-vera" );
      bi_put( "GDK_DPI_SCALE", "1.2" );
      bi_put( "GDMSESSION", "lightdm-xsession" );
      bi_put( "GIT_AUTHOR_EMAIL", "thea@ncorehpc.com" );
      bi_put( "GIT_AUTHOR_NAME", "Thea-Martine Lintault" );
      bi_put( "GIT_COMMITTER_EMAIL", "thea@ncorehpc.com" );
      bi_put( "GIT_COMMITTER_NAME", "Thea-Martine Lintault" );
      bi_put( "GLFW_IM_MODULE", "ibus" );
      bi_put( "GNUPLOT_DEFAULT_GDFONT", "Vera" );
      bi_put( "GPG_AGENT_INFO", "/run/user/1000/gnupg/S.gpg-agent:0:1" );
      bi_put( "GPG_TTY", "/dev/pts/2" );
      bi_put( "GTK_IM_MODIFIERS", "fcitx" );
      bi_put( "GTK_IM_MODULE", "fcitx" );
      bi_put( "HLL", "C" );
      bi_put( "HOME", "/home/thea" );
      bi_put( "HOST", "hokuto" );
      bi_put( "HOSTNAME", "hokuto" );
      bi_put( "I3SOCK", "/run/user/1000/i3/ipc-socket.2527" );
      bi_put( "IFS", "' 	" );
      bi_put( "INFOPATH", "/usr/local/share/info:/usr/share/info" );
      bi_put( "IN_SCREEN", "" );
      bi_put( "KERNELBASEDIR", "/home/thea/build/release-atomics/kernel" );
      bi_put( "KERNELDIR", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ" );
      bi_put( "KERNELNAME_FULL", "" );
      bi_put( "KITTY_INSTALLATION_DIR", "/home/thea/.local/kitty.app/lib/kitty" );
      bi_put( "KITTY_PID", "4425" );
      bi_put( "KITTY_WINDOW_ID", "2" );
      bi_put( "LC_ALL", "ja_JP.utf8" );
      bi_put( "LC_CTYPE", "ja_JP.utf8" );
      bi_put( "LD_LIBRARY_PATH", "/home/thea/.local/lib:/usr/local/cuda-7.0/lib64" );
      bi_put( "LESS", "-a -h100 -i -j15 -M -q -R -W -y100 -X" );
      bi_put( "LESSCOLORIZER", "/home/thea/bin/code2color" );
      bi_put( "LESSKEY", "/home/thea/.less" );
      bi_put( "LESSOPEN", "|/home/thea/bin/lesspipe.sh %s" );
      bi_put( "LOCAL_BENCHITC_COMPILER", "riscv64-unknown-linux-gnu-clang  -O2  -DDEBUGLEVEL=0" );
      bi_put( "LOCAL_KERNEL_COMPILER", "riscv64-unknown-linux-gnu-clang" );
      bi_put( "LOCAL_KERNEL_COMPILERFLAGS", " -O0 -I. -I/home/thea/build/release-atomics -I/home/thea/build/release-atomics/tools -I/home/thea/build/release-atomics/tools/hw_detect" );
      bi_put( "LOCAL_LINKERFLAGS", "-lm " );
      bi_put( "LOGNAME", "thea" );
      bi_put( "LSBIN", "/home/thea/bin/Linux-x86_64" );
      bi_put( "LS_COLORS", "" );
      bi_put( "MANPATH", "/home/thea/.local/share/man:" );
      bi_put( "MOZ_CRASHREPORTER", "0" );
      bi_put( "MOZ_DATA_REPORTING", "0" );
      bi_put( "MOZ_GTK_TITLEBAR_DECORATION", "client" );
      bi_put( "MOZ_SERVICES_HEALTHREPORT", "0" );
      bi_put( "MOZ_TELEMETRY_REPORTING", "0" );
      bi_put( "NO_AT_BRIDGE", "1" );
      bi_put( "NPM_CONFIG_PREFIX", "/home/thea/.local/node_modules" );
      bi_put( "OLDCWD", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ" );
      bi_put( "OLDIR", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ" );
      bi_put( "OLDPWD", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ" );
      bi_put( "OMP_DYNAMIC", "FALSE" );
      bi_put( "OMP_NESTED", "FALSE" );
      bi_put( "OMP_NUM_THREADS", "1" );
      bi_put( "OPTIND", "1" );
      bi_put( "OSTYPE", "linux-gnu" );
      bi_put( "PAGER", "less" );
      bi_put( "PATH", "/home/thea/build/release-atomics/tools:/home/thea/.local/bin:/home/thea/bin/Linux:/home/thea/mytools/bin:/usr/local/cuda-7.0/bin:/home/thea/bin:/usr/bin:/usr/local/bin:/bin:/usr/bin/X11:/etc:/usr/local/sbin:/usr/sbin:/sbin:/home/ian/tau-2.19.1/x86_64/bin:/home/thea/ti/xdctools_3_24_06_63:/home/thea/ti/TI_CGT_C6000_7.5.0A12317/bin:/opt/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu/bin/:/opt/riscv64-unknown-linux-gnu-toolsuite-13.0.0-2021.11.4-preview-x86_64-linux-redhat8/bin:/home/thea/.local/texlive/2019/bin/x86_64-linux:/home/thea/ddt/bin:/home/thea/tau/x86_64/bin:/opt/Zotero_linux-x86_64:/opt/GitAhead:/home/thea/Qt/5.15.2/gcc_64/bin:/opt/scilab-6.1.0/bin:/opt/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/bin:/usr/local/plan9/bin:/home/thea/.fzf/bin" );
      bi_put( "PERL5LIB", "" );
      bi_put( "PKG_CONFIG_PATH", "/home/thea/.local/lib/pkgconfig:/home/thea/.local/lib/pkgconfig:" );
      bi_put( "PLAN9", "/usr/local/plan9" );
      bi_put( "PPID", "591507" );
      bi_put( "PS1", "$ " );
      bi_put( "PS2", "> " );
      bi_put( "PS4", "+ " );
      bi_put( "PWD", "/home/thea/build/release-atomics/tools" );
      bi_put( "PYTHONPATH", "/usr/lib/python3.7/site-packages" );
      bi_put( "PYTHON_KEYRING_BACKEND", "keyring.backends.null.Keyring" );
      bi_put( "QT_ACCESSIBILITY", "1" );
      bi_put( "QT_IM_MODIFIERS", "fcitx" );
      bi_put( "QT_IM_MODULE", "fcitx" );
      bi_put( "QT_QPA_PLATFORMTHEME", "qt5ct" );
      bi_put( "QT_SCALE_FACTOR", "1.2" );
      bi_put( "RISCV_OPENOCD_PATH", "/opt/riscv-openocd-0.10.0-2021.06.0-x86_64-linux-ubuntu14" );
      bi_put( "RISCV_PATH", "/opt/riscv64-unknown-elf-toolsuite-12.0.0-2021.06.3-x86_64-linux-ubuntu14" );
      bi_put( "RUBYLIB", "" );
      bi_put( "SCREENRC", "/home/thea/.screenrc" );
      bi_put( "SCRIPTNAME", "COMPILE.SH" );
      bi_put( "SHELL", "/bin/zsh" );
      bi_put( "SHELLSCRIPT_DEBUG", "0" );
      bi_put( "SHLVL", "2" );
      bi_put( "SSH_ASKPASS", "/usr/bin/ssh-askpass" );
      bi_put( "SSH_AUTH_SOCK", "/run/user/1000/gnupg/S.gpg-agent.ssh" );
      bi_put( "TAU_MAKEFILE", "/home/thea/x86_64/lib/Makefile.tau-icpc-papi-pthread" );
      bi_put( "TERM", "xterm-kitty" );
      bi_put( "TERMINFO", "/home/thea/.local/kitty.app/lib/kitty/terminfo" );
      bi_put( "TRUNK", "trunk" );
      bi_put( "USER", "thea" );
      bi_put( "USERNAME", "thea" );
      bi_put( "VISUAL", "/usr/bin/vim" );
      bi_put( "VSLICKXNOPLUSNEWMSG", "1" );
      bi_put( "WINDOWID", "125829131" );
      bi_put( "XAUTHORITY", "/home/thea/.Xauthority" );
      bi_put( "XDG_DATA_DIRS", "/home/thea/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share" );
      bi_put( "XDG_GREETER_DATA_DIR", "/var/lib/lightdm/data/thea" );
      bi_put( "XDG_RUNTIME_DIR", "/run/user/1000" );
      bi_put( "XDG_SEAT", "seat0" );
      bi_put( "XDG_SEAT_PATH", "/org/freedesktop/DisplayManager/Seat0" );
      bi_put( "XDG_SESSION_CLASS", "user" );
      bi_put( "XDG_SESSION_DESKTOP", "lightdm-xsession" );
      bi_put( "XDG_SESSION_ID", "2" );
      bi_put( "XDG_SESSION_PATH", "/org/freedesktop/DisplayManager/Session0" );
      bi_put( "XDG_SESSION_TYPE", "x11" );
      bi_put( "XDG_VTNR", "7" );
      bi_put( "XMODIFIERS", "@im=fcitx" );
      bi_put( "XSCREEN", "1" );
      bi_put( "ZDOTDIR", "/home/thea" );
      bi_put( "ZDOTDIRPATH", "/home/thea" );
      bi_put( "ZDOTDIRREVPATH", "/home/thea" );
      bi_put( "ZDOTUSER", "thea" );
      bi_put( "ZDOT_FIND_HOOKS", "/home/thea/.zsh/functions/find_hooks" );
      bi_put( "ZDOT_RUN_HOOKS", "/home/thea/.zsh/functions/run_hooks" );
      bi_put( "_", "/home/thea/build/release-atomics/kernel/arch_riscv64/memory_latency/C/pthread/0/cmpxchg128-succ/./COMPILE.SH" );
      bi_put( "_CMDLINE_VARLIST", "BENCHIT_KERNELBINARY BENCHIT_KERNELBINARY_ARGS BENCHIT_CMDLINE_ARG_FILENAME_COMMENT BENCHIT_CMDLINE_ARG_PARAMETER_FILE BENCHIT_CMDLINE_ARG_IGNORE_PARAMETER_FILE BENCHIT_NODENAME BENCHIT_CROSSCOMPILE BENCHIT_CMDLINE_ARG_NUM_CPUS BENCHIT_CMDLINE_ARG_NUM_PROCESSES BENCHIT_CMDLINE_ARG_NUM_THREADS_PER_PROCESS BENCHIT_CMDLINE_ARG_RUN_CLEAN BENCHIT_CMDLINE_ARG_RUN_COREDUMPLIMIT BENCHIT_CMDLINE_ARG_RUN_EMAIL_ADDRESS BENCHIT_CMDLINE_ARG_RUN_MAX_MEMORY BENCHIT_CMDLINE_ARG_RUN_QUEUENAME BENCHIT_CMDLINE_ARG_RUN_QUEUETIMELIMIT BENCHIT_CMDLINE_ARG_RUN_REDIRECT_CONSOLE BENCHIT_CMDLINE_ARG_RUN_TEST BENCHIT_CMDLINE_ARG_RUN_USE_MPI BENCHIT_CMDLINE_ARG_RUN_USE_OPENMP " );
      bi_put( "_JAVA_AWT_WM_NONREPARENTING", "1" );
      bi_put( "_JAVA_OPTIONS", "-Dawt.useSystemAAFontSettings=on -Dsun.java2d.uiScale=2.0 -Dui.fonts.useDefaultAsInfo=true" );
      bi_put( "_VARLIST", "'BENCHITROOT" );
      bi_put( "__GL_SYNC_TO_VBLANK", "0" );
      bi_put( "__ZSH_GIT_BRANCH", "" );
      bi_put( "__ZSH_GIT_DIR", "" );
      bi_put( "__ZSH_GIT_STATE", "" );
      bi_put( "__ZSH_GIT_VARS_INVALID", "1" );
      bi_put( "archtype", "Linux-x86_64" );
      bi_put( "c2cbold", "1" );
      bi_put( "myfile", "tools/stringlib.h" );
      bi_put( "myval", "" );
      bi_put( "myvar", "BENCHIT_CMDLINE_ARG_RUN_USE_OPENMP" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_CBLAS", "NO REVISION, UNABLE TO READ (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_FEATURES", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BENCHSCRIPT_H", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_CHANGE_SH_SH", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_ALIGNED_MEMORY_H", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_REFERENCE_RUN_SH", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_CONFIGURE", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_ENVIRONMENTS", "NO REVISION, UNABLE TO READ (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BIENVHASH_H", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_COMPILERVERSION", "NO REVISION, UNABLE TO READ (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_FILEVERSION_C", "*/ (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_HELPER_SH", "1.14 (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_FIRSTTIME", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_FILEVERSION", "NO REVISION (Tue Jul 26 11:47:32 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BIENVHASH_TEMPLATE_C", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_TMP_ENV", "NO REVISION (Tue Jul 26 11:47:32 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BIENVHASH_TEMPLATE_JAVA", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BENCHSCRIPT_C", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_STRINGLIB_H", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_HW_DETECT", "NO REVISION, UNABLE TO READ (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_LOC_REPL", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BMERGE_SH", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_LOC_CONVERT_SH", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_CMDLINEPARAMS", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_QUICKVIEW_SH", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_ERROR_H", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_ENVHASHBUILDER", "NO REVISION (Tue Jul 26 11:47:32 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_ENVHASHBUILDER_C", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_STRINGLIB_C", "NO REVISION (Sat Jun 18 09:47:39 2022)" );
      bi_put( "BENCHIT_KERNEL_FILE_VERSION_BLAS", "NO REVISION, UNABLE TO READ (Sat Jun 18 09:47:39 2022)" );
   }
}
