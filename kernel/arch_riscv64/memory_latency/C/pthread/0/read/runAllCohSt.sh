#!/bin/bash
BINARYNAME="arch_riscv64.memory_latency.C.pthread.0.read.0"

# set all Parameters that might have been changed
sed -i 's/BENCHIT_KERNEL_CPU_LIST=".*"/BENCHIT_KERNEL_CPU_LIST="0,2,3"/g' PARAMETERS
sed -i 's/BENCHIT_KERNEL_HUGEPAGES=0/BENCHIT_KERNEL_HUGEPAGES=0/g' PARAMETERS
# turn on/off cache flushes
sed -i 's/BENCHIT_KERNEL_FLUSH_L1=./BENCHIT_KERNEL_FLUSH_L1=1/g' PARAMETERS
sed -i 's/BENCHIT_KERNEL_FLUSH_L2=./BENCHIT_KERNEL_FLUSH_L2=1/g' PARAMETERS
sed -i 's/BENCHIT_KERNEL_FLUSH_L3=./BENCHIT_KERNEL_FLUSH_L3=1/g' PARAMETERS

#Modified
sed -i 's/BENCHIT_KERNEL_USE_MODE="."/BENCHIT_KERNEL_USE_MODE="M"/g' PARAMETERS
sh COMPILE.SH
(cd ../../../../../../../
sh RUN.SH $BINARYNAME)

#Exclusive
sed -i 's/BENCHIT_KERNEL_USE_MODE="."/BENCHIT_KERNEL_USE_MODE="E"/g' PARAMETERS
sh COMPILE.SH
(cd ../../../../../../../
sh RUN.SH $BINARYNAME)

#Invalid
sed -i 's/BENCHIT_KERNEL_USE_MODE="."/BENCHIT_KERNEL_USE_MODE="I"/g' PARAMETERS
sh COMPILE.SH
(cd ../../../../../../../
sh RUN.SH $BINARYNAME)

# also Shared and Forward if the benchmark supports it

if grep  "#S: Shared,    shares unmodified cachelines with another CPU, other CPU reads last" PARAMETERS
then
	echo "TRYING TO EXECUTE SHARED AND FORWARD"
	# prepare for S and F state
	sed -i 's/BENCHIT_KERNEL_CPU_LIST=".*"/BENCHIT_KERNEL_CPU_LIST="0,2"/g' PARAMETERS
	sed -i 'BENCHIT_KERNEL_SHARE_CPU="."/BENCHIT_KERNEL_SHARE_CPU="3"/g' PARAMETERS
	# Shared
	sed -i 's/BENCHIT_KERNEL_USE_MODE="."/BENCHIT_KERNEL_USE_MODE="S"/g' PARAMETERS
	sh COMPILE.SH
	(cd ../../../../../../../
 	sh RUN.SH $BINARYNAME)
	# Forward
	sed -i 's/BENCHIT_KERNEL_USE_MODE="."/BENCHIT_KERNEL_USE_MODE="F"/g' PARAMETERS
	sh COMPILE.SH
	(cd ../../../../../../../
	 sh RUN.SH $BINARYNAME)
else
	echo "NO SHARED AND FORWARD FOR THIS KERNEL"
fi



