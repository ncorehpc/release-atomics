/********************************************************************
 * BenchIT - Performance Measurement for Scientific Applications
 * Contact: developer@benchit.org
 *
 * $Id$
 * $URL$
 * For license details see COPYING in the package base directory
 *******************************************************************/


#define _GNU_SOURCE
#include <sched.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>

#include "arch.h"
#include "cpu.h"
#include "riscv64.h"

#define MAX_OUTPUT 512

static char output[MAX_OUTPUT];

/**
 * initializes cpuinfo-struct
 * @param print detection-summary is written to stdout when !=0
 */
void init_cpuinfo(cpu_info_t *cpuinfo,int print)
{
  int i,j;
  char *tmp,*tmp2;
  int pagesize_id;

  /* initialize data structure */
  memset(cpuinfo,0,sizeof(cpu_info_t));
  strcpy(cpuinfo->architecture,"unknown\0");
  strcpy(cpuinfo->vendor,"unknown\0");
  strcpy(cpuinfo->model_str,"unknown\0");

  /* print a summary */
  if (print)
  {
    fflush(stdout);
    printf("\n  hardware detection summary:\n");
    printf("    architecture:   %s\n",cpuinfo->architecture);
    printf("    vendor:         %s\n",cpuinfo->vendor);
    printf("    processor-name: %s\n",cpuinfo->model_str);
    printf("    model:          Family %i, Model %i, Stepping %i\n",cpuinfo->family,cpuinfo->model,cpuinfo->stepping);
  }
  fflush(stdout);
}

/**
 * pin process to a cpu
 */
int cpu_set(int id)
{
  cpu_set_t  mask;

  CPU_ZERO( &mask );
  CPU_SET(id , &mask );
  return sched_setaffinity(0,sizeof(cpu_set_t),&mask);
}

/**
 * check if a cpu is allowed to be used
 */
int cpu_allowed(int id)
{
  cpu_set_t  mask;

  CPU_ZERO( &mask );
  if (!sched_getaffinity(0,sizeof(cpu_set_t),&mask))
  {
    return CPU_ISSET(id,&mask);
  }
  return 0;
}

/**
 * flushes content of buffer from all cache-levels
 * @param buffer pointer to the buffer
 * @param size size of buffer in Bytes
 * @return 0 if successful
 *         -1 if not available
 */
int inline clflush(void* buffer,unsigned long long size,cpu_info_t cpuinfo)
{
  #if defined (__x86_64__)
  unsigned long long addr,passes,linesize;

  if(!(cpuinfo.features&CLFLUSH) || !cpuinfo.clflush_linesize) return -1;

  addr = (unsigned long long) buffer;
  linesize = (unsigned long long) cpuinfo.clflush_linesize;

   __asm__ __volatile__("mfence;"::);
   for(passes = (size/linesize);passes>0;passes--)
   {
      __asm__ __volatile__("clflush (%%rax);":: "a" (addr));
      addr+=linesize;
   }
   __asm__ __volatile__("mfence;"::);
  #endif

  return 0;
}

/*
 *  the remaining functions are currently not used
 */
#if 0
/*
 * misuses non temporal stores to flush cache
 * alternative for clflush
 * @param buffer pointer to the buffer
 * @param size size of buffer in Bytes
 * @return 0 if successful
 *         -1 if not available
 */
int inline write_nt(void* buffer,unsigned long long size,cpu_info_t cpuinfo)
{
  #if defined (__x86_64__)
  unsigned long long addr,passes,linesize;

  if(!(cpuinfo.features&SSE2)) return -1;

  addr = (unsigned long long) buffer;
  linesize = 16;

  addr=addr&(0xffffffffffffff00);

   __asm__ __volatile__("mfence;"::);
   for(passes = (size/linesize)-1;passes>0;passes--)
   {
      __asm__ __volatile__("movdqa (%%rax),%%xmm0;movntdq %%xmm0, (%%rax);":: "a" (addr): "%xmm0");
      addr+=linesize;
   }
   __asm__ __volatile__("mfence;"::);
  #endif

  return 0;
}
/**
 * prefetches content of buffer
 * @param buffer pointer to the buffer
 * @param size size of buffer in Bytes
 * @return 0 if successful
 *         -1 if not available
 */
int inline prefetch(void* buffer,unsigned long long size, cpu_info_t cpuinfo)
{
  #if defined (__x86_64__)
  unsigned long long addr,passes,linesize;
  int i;

  if(!(cpuinfo.features&SSE)) return -1;

  addr = (unsigned long long) buffer;
  linesize = 256;
  for (i=cpuinfo.Cachelevels;i>0;i--)
  {
    if (cpuinfo.Cacheline_size[i-1]<linesize) linesize=cpuinfo.Cacheline_size[i-1];
  }

  for(passes = (size/linesize);passes>0;passes--)
  {
    __asm__ __volatile__("prefetcht0 (%%rax);":: "a" (addr));
    addr+=linesize;
  }
  #endif

  return 0;
}
#endif
