#backup old results
(cd ../../output/ 
sh BackupArch.sh)
#run all benchmarks in all coherency states
find . -name 'runAllCohSt.sh' -execdir sh {} \;
# plot all results 
(cd ../../output/
sh fixAndPlot.sh)
# create PDF of all Plots
(cd ../../output/latex 
sh createPdf.sh)
