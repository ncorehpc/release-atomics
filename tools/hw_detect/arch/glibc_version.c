#include <gnu/libc-version.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

double glibc_version()
{
  double res=0.0;
  char *version;
  const char *tmp;
  int i,j=0,dot=0,length;

  tmp=gnu_get_libc_version();
  length=strlen(tmp);
  version=malloc((length+1)*sizeof(char));

  for (i=0;i<length;i++)
  {
    if ((tmp[i]>='0')&&(tmp[i]<='9')) version[j++]=tmp[i];
    else if (tmp[i]=='.') {if (!dot) {dot=1;version[j++]=tmp[i];}}
    else {version[j]='\0';break;}
  }
  res = strtod(version,NULL);
  return res;
}

int main(int argc,char **argv)
{
  double test;

  if (argc==1) printf("%1.2f\n",glibc_version());
  if (argc==2)
  {
    test=strtod(argv[1],NULL);
    if (test<glibc_version()) printf("ok\n");
    else printf("fail\n");
  }
}
