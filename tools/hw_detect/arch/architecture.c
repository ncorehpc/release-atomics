/** B e n c h I T - Performance Measurement for Scientific Applications
* @file architecture.c
*  implements the commandline interface
*  calls architecture specific implementations
*
* Author: Daniel Molka (daniel.molka@zih.tu-dresden.de)
*/

#include <stdlib.h>
#include <unistd.h>

#include "cpu.h"

/**
 * display usage information
 */
static void usage()
{
     printf("usage: cpuinfo [option [arg]] [-cpu <ID>]\n");
     printf("options:\n");
     printf("  --help:                  this help screen\n");
     /*TODO --info <cmd> with further help*/
     printf("  --all:                   System summary\n");
     printf("  cpu_name:                Name of the CPU\n");
     printf("  cpu_vendor:              Vendor of the CPU\n");
     printf("  cpu_codename:            Codename of the CPU\n");
     printf("  cpu_isa:                 Instruction Set of the CPU\n");
     printf("  cpu_family:              Family of the CPU\n");
     printf("  cpu_model:               Model of the CPU\n");
     printf("  cpu_stepping:            Stepping of the CPU\n");
     printf("  cpu_gate_length:         manufacturing process in nm\n");
     printf("  cpu_features:            ISA extensions of the CPU\n");
     printf("  cpu_clockrate:           Clockrate of the CPU\n");
     printf("  cpu_clockrate_no_check:  Clockrate of the CPU (allows use of unreliable sources)\n");
     printf("  frequency_scaling:       supported frequencies and governor\n");
     printf("  timestamp:               returns a timestamp from internal counter register\n");
     printf("  get_cpu_id:              ID of the CPU the programm is currently running on\n");
     printf("  get_core_id:             ID of the core the programm is currently running on\n");
     printf("  num_packages:            Number of Packages (Sockets) in the System\n");
     printf("  get_package_id:          ID of the physical Packages the programm is currently running on\n");
     printf("  num_numa_nodes:          Number of NUMA Nodes in the System (1 -> UMA)\n");
     printf("  get_numa node :          NUMA Node the programm is currently running on\n");
     printf("  num_cores_per_package:   Number of Cores in a Package  (>1 -> Multicore)\n");
     printf("  num_threads_per_core:    Number of Threads per Core    (>1 -> SMT support)\n");
     printf("  num_threads_per_package: Number of Threads per Package (>1 -> SMT or Multicore)\n");
     printf("  num_caches:              Number of Caches in the CPU (per Core)\n");
     printf("  cache_info <ID>:         Type of the Cache (0 <= ID < num_caches)\n");
     printf("  num_tlbs:                Number of TLBs in the CPU (per Core)\n");
     printf("  tlb_info <ID>:           Type of the TLB (0 <= ID < num_tlbs)\n");
     printf("  physical_address_length  supported physical address length\n");
     printf("  virtual_address_length   supported virtual address length\n");
     printf("  pagesizes                supported page sizes\n");
     printf("-cpu <ID>:\n");
     printf("  run on specific CPU: 0 <= ID < number of CPUs\n");
}

/**
 * list information about a certain CPU
 */
static void show_cpu_info(char* tmp,int i)
{
   unsigned long long res;
   long long size;
   int j,num;
   char output[MAX_OUTPUT];
   char *pos,*old;

   if (i==-1) printf("CPU properties:\n");
   else printf("CPU%i:\n",i);

   res=get_core_id(i);
   if (res!=-1) printf("  Core ID:          %llu\n",res);
   else printf("  Core ID:          n/a\n");
   res=get_pkg(i);
   if (res!=-1) printf("  Physical Package: %llu\n",res);
   else printf("  Physical Package: n/a\n");
   res=num_numa_nodes();
   if ((res!=-1)&&(res!=0)&&(res!=1))
   {
     res=get_numa_node(i);
     if (res!=-1) printf("  NUMA Node:        %llu\n",res);
     else printf("  NUMA Node:        n/a\n");
   }

   strcpy(tmp,"n/a");get_cpu_vendor(tmp);printf("  Vendor:           %s\n",tmp);
   strcpy(tmp,"n/a");get_cpu_name(tmp);printf("  Name:             %s\n",tmp);

   res=get_cpu_family();
   if (res!=-1) printf("  Model:            Family: %llu, ",res);
   else printf("  Model:            Family: n/a, ");
   res=get_cpu_model();
   if (res!=-1) printf("Model: %llu, ",res);
   else printf("Model: n/a, ");
   res=get_cpu_stepping();
   if (res!=-1) printf("Stepping: %llu\n",res);
   else printf("Stepping: n/a\n");

   res=get_cpu_clockrate(1,i);
   if (res!=0) printf("  Clockrate:        %llu MHz\n",res/1000000);
   else
   {
      res=get_cpu_clockrate(0,i);
      if (res!=0) printf("  Clockrate:        %llu MHz (Warning: estimated using unreliable source)\n",res/1000000);
      else printf("  Clockrate:        n/a\n");
   }

   res=supported_frequencies(i,tmp);
   if (res!=-1)
   {
     printf("  Supported Freqs:  %s",tmp);
     res=scaling_governor(i,tmp);
     if (res!=-1) printf(", Governor: %s ",tmp);
     res=scaling_driver(i,tmp);
     if (res!=-1) printf(", Driver: %s ",tmp);
     printf("\n");
   }

   strcpy(tmp,"n/a");get_cpu_codename(tmp);printf("  Codename:         %s\n",tmp);
   res=get_cpu_gate_length();
   if (res!=-1) printf("  Gate Length:      %llu nm\n",res);
   else printf("  Gate Length:      n/a\n");

   strcpy(tmp,"n/a");get_cpu_isa_extensions(tmp);
   printf("  ISA extensions:   ");
   pos=tmp;
   do
   {
     old=pos-1;
     pos=strstr(pos," ");
     if ((((pos!=NULL)&&(strstr(pos,")")<strstr(pos,"(")))||((pos!=NULL)&&(strstr(pos,")")!=NULL)&&(strstr(pos,"(")==NULL))) && (strstr(pos,")")-tmp>70))
     {
        *old='\0';
        printf("%s\n",tmp);
        tmp=old+1;
        if (strlen(tmp)>0) printf("                    ");
     }
     if ((pos!=NULL)&&(pos-tmp>70))
     {
        *pos='\0';pos++;
        printf("%s\n",tmp);
        tmp=pos;
        if (strlen(tmp)>0) printf("                    ");
     }
     else if ((pos==NULL)||((pos!=NULL)&&(strlen(pos)==1)))
     {
        printf("%s\n",tmp);
        if (pos!=NULL) pos++;
     }
     else pos++;
   }
   while ((pos!=NULL)&&(*pos!='\0'));
   res=get_phys_address_length();
   if (res!=-1) printf("  Phys Addr Length: %llu Bit\n",res);
   else printf("  Phys Addr Length: n/a\n");
   res=get_virt_address_length();
   if (res!=-1) printf("  Virt Addr Length: %llu Bit\n",res);
   else printf("  Virt Addr Length: n/a\n");

   num=num_pagesizes();
   if (num!=-1)
   {
    printf("  Pagesizes:        ");
    for(j=0;j<num;j++)
    {
     size=pagesize(j);
     if (size!=-1)
     {
       if (size>=1073741824)printf("%lliG ",size>>30);
       else if (size>1048576)printf("%lliM ",size>>20);
       else if (size>1024)printf("%lliK ",size>>10);
       else printf("%llu Byte ",res);
     }
    }
    printf("\n");
   }

   res=num_caches(i);
   if (res!=-1)
   {
     printf("  Caches:\n");
     for (j=0;j<res;j++)
     {
        sprintf(output,"n/a");
        if (cache_info(i,j,output)!=-1) printf("   - %s\n",output);
     }
   }
   res=num_tlbs(i);
   if (res!=-1)
   {
     printf("  TLBs:\n");
     for (j=0;j<res;j++)
     {
        sprintf(output,"n/a");
        if (tlb_info(i,j,output)!=-1) printf("   - %s\n",output);
     }
   }
}

int main(int argc, char** argv)
{
  char tmp[MAX_OUTPUT];
  strcpy((char*)&tmp,"n/a");
  unsigned long long res;
  int cpu=-1,err,i;
  char *endptr;

  /* system summary */
  if ((argc==1)||(!strcmp(argv[1],"--all")))
  {
     get_architecture(tmp);printf("Architecture:   %s\n",tmp);
     printf("Number of (logical) CPUs: %i\n",num_cpus());
     res=num_packages();
     if (res!=-1) printf(" - Number of packages (sockets):   %llu\n",res);
     else printf(" - Number of packages (sockets):   n/a\n");
     res=num_cores_per_package();
     if (res!=-1) printf(" - Number of cores per package:    %llu\n",res);
     else printf(" - Number of cores per package:    n/a\n");
     res=num_threads_per_core();
     if (res!=-1) printf(" - Number of threads per core:     %llu\n",res);
     else printf(" - Number of threads per core:     n/a\n");
     res=num_threads_per_package();
     if (res!=-1) printf(" - Number of threads per package:  %llu\n",res);
     else printf(" - Number of threads per package:  n/a\n");
     res=num_numa_nodes();
     if ((res!=-1)&&(res!=0)&&(res!=1)) printf(" - Number of NUMA Nodes:           %llu\n",res);
     #if defined(AFFINITY)
     for (i=0;i<num_cpus();i++)
     {
        err=set_cpu(i);
        if(err==-2) printf("CPU%i: not allowed to run on\n",i);
        else if (err) printf("CPU%i: could not set affinity\n",i);
        else show_cpu_info(&tmp[0],i);
        restore_affinity();
     }
     #else
       if (num_cpus()>1) printf("CPU affinity not available, running on any CPU\n");
       show_cpu_info(&tmp[0],cpu);
     #endif
     return 0;
  }

  if (!strcmp(argv[1],"--help"))
  {
     usage();
     return 0;
  }

  if ((argc==4)&&(!(strcmp(argv[2],"-cpu"))))
  {
    cpu=(int)strtol(argv[3],&endptr,10);
    if ((cpu<0)||(cpu>num_cpus()-1)||((*endptr)!='\0'))
    {
      printf("Error: unknown CPU: %s\n",argv[3]);
      return -1;
    }
    err=set_cpu(cpu);
    if(err)
    {
       printf("Error: CPU affinity not available\n");
       return -1;
    }
  }

  if ((argc==5)&&(!(strcmp(argv[3],"-cpu"))))
  {
    cpu=(int)strtol(argv[4],&endptr,10);
    if ((cpu<0)||(cpu>num_cpus()-1)||((*endptr)!='\0'))
    {
      printf("Error: unknown CPU: %s\n",argv[4]);
      return -1;
    }
    err=set_cpu(cpu);
    if(err)
    {
       printf("Error: CPU affinity not available\n");
       return -1;
    }
  }

  /*TODO other illegal input */
  if (((argc>4)&&(!(strcmp(argv[2],"-cpu"))))||((argc>5)&&(!(strcmp(argv[3],"-cpu"))))||(argc>6))
  {
     printf("Error: illegal command\n");
     usage();
     return -1;
  }

  if(!strcmp(argv[1],"cpu_isa"))
  {
     get_architecture((char*)&tmp);
     printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_vendor"))
  {
     get_cpu_vendor((char*)&tmp);
     printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_name"))
  {
     get_cpu_name((char*)&tmp);
     printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_codename"))
  {
     get_cpu_codename((char*)&tmp);
     printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_family"))
  {
     res=get_cpu_family();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_model"))
  {
     res=get_cpu_model();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_stepping"))
  {
     res=get_cpu_stepping();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_gate_length"))
  {
     res=get_cpu_gate_length();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_features"))
  {
     get_cpu_isa_extensions(tmp);
     printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_clockrate"))
  {
     res=get_cpu_clockrate(1,cpu);
     if (res!=0) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cpu_clockrate_no_check"))
  {
     res=get_cpu_clockrate(0,cpu);
     if (res!=0) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"frequency_scaling"))
  {
     res=supported_frequencies(cpu,tmp);
     if (res!=-1)
     {
       printf("Supported Frequencies: %s",tmp);
       res=scaling_governor(cpu,tmp);
       if (res!=-1) printf(", Governor: %s",tmp);
       res=scaling_driver(cpu,tmp);
       if (res!=-1) printf(", Driver: %s",tmp);
       printf("\n");
     }
     else printf("n/a\n");
     return 0;
  }

  if(!strcmp(argv[1],"timestamp"))
  {
     res=timestamp();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"get_cpu_id"))
  {
     res=get_cpu();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"get_core_id"))
  {
     res=get_core_id(cpu);
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"get_package_id"))
  {
     res=get_pkg(cpu);
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_packages"))
  {
     res=num_packages();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_numa_nodes"))
  {
     res=num_numa_nodes();
     if ((res!=-1)&&(res!=0)) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"get_numa_node"))
  {
     res=get_numa_node(cpu);
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_cores_per_package"))
  {
     res=num_cores_per_package();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_threads_per_core"))
  {
     res=num_threads_per_core();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_threads_per_package"))
  {
     res=num_threads_per_package();
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_caches"))
  {
     res=num_caches(cpu);
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"cache_info"))
  {
     res=cache_info(cpu,atoi(argv[2]),tmp);
     if (res!=-1) printf("%s\n",tmp);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"num_tlbs"))
  {
     res=num_tlbs(cpu);
     if (res!=-1) printf("%llu\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"tlb_info"))
  {
     res=tlb_info(cpu,atoi(argv[2]),tmp);
     if (res!=-1) printf("%s\n",tmp);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"physical_address_length"))
  {
     res=get_phys_address_length();
     if (res!=-1) printf("%llu Bit\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"virtual_address_length"))
  {
     res=get_virt_address_length();
     if (res!=-1) printf("%llu Bit\n",res);
     else printf("%s\n",tmp);
     return 0;
  }

  if(!strcmp(argv[1],"pagesizes"))
  {
     int i,num;
     long long size;

     num=num_pagesizes();
     if (num!=-1)
     {
      for(i=0;i<num;i++)
      {
       size=pagesize(i);
       if (size!=-1)
       {
         if (size>=1073741824)printf("%lliG ",size>>30);
         else if (size>1048576)printf("%lliM ",size>>20);
         else if (size>1024)printf("%lliK ",size>>10);
         else printf("%llu Byte ",res);
       }
      }
      printf("\n");
     }
     return 0;
  }

  if ((argc==3)&&(!strcmp(argv[1],"-cpu")))
  {
   cpu=(int)strtol((argv[2]),&endptr,10);
   if ((cpu>=0)&&(cpu<num_cpus())&&(*endptr=='\0'))
   {
     err=set_cpu(cpu);

     if(err)
     {
        printf("Error: CPU affinity not available\n");
        return -1;
     }

     show_cpu_info(&tmp[0],cpu);
     return 0;
   }
  }

  printf("Error: unknown option: %s\n",argv[1]);
  usage();
  return -1;
}

