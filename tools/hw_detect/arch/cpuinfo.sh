OLD_PWD=${PWD};
if [ "${HW_DETECT_BASE}" != "" ];then cd "${HW_DETECT_BASE}/arch";fi
if [ "${BENCHITROOT}" != "" ];then cd "${BENCHITROOT}/tools/hw_detect/arch";fi

if [ -x cpuinfo ] || [ -r architecture.c ]; then
 if [ ! -x cpuinfo ]; then
  sh compile.sh
 fi
 NAME="`./cpuinfo cpu_name`"
 FAMILY="`./cpuinfo cpu_family`"
 MODEL="`./cpuinfo cpu_model`"
 CORES_PER_PACKAGE="`./cpuinfo num_cores_per_package`"
 if [ "$1" = "-codename" ]; then
  echo "`awk -F\"\t\"* -v arch=\"${NAME}\" -v family=\"${FAMILY}\" -v model=\"${MODEL}\" -v request=\"codename\" -f properties.awk x86_properties.list`"
 fi
 if [ "$1" = "-process" ]; then
  echo "`awk -F\"\t\"* -v arch=\"${NAME}\" -v family=\"${FAMILY}\" -v model=\"${MODEL}\" -v request=\"node\" -f properties.awk x86_properties.list`"
 fi
 if [ "$1" = "-arch_short" ]; then
  echo "`awk -F\"\t\"* -v arch=\"${NAME}\" -v cores=\"${CORES_PER_PACKAGE}\" -f arch_short.awk arch_short.list`"
 fi
 cd "${OLD_PWD}"
fi
