DEFINES=""
if [ -r "/usr/include/gnu/libc-version.h" ];then
 DEFINES="${DEFINES} -DGLIBC_VERSION"
 gcc -o glibc_version glibc_version.c
 echo "         checking for glibc version:        found version `./glibc_version`"
 if [ "`./glibc_version 2.34`" = "ok" ] && [ -r "/usr/include/sched.h" ];then 
   DEFINES="${DEFINES} -DAFFINITY"
   echo "         checking for sched_setaffinity:    ok"
 else
   echo "         checking for sched_setaffinity:    failed"
 fi
 if [ "`./glibc_version 2.6`" = "ok" ] && [ -r "/usr/include/utmpx.h" ];then 
   DEFINES="${DEFINES} -DSCHED_GETCPU"
   echo "         checking for sched_getcpu():       ok"
 else
   echo "         checking for sched_getcpu():       failed"
 fi
else
 echo "         checking for glibc version:        not found"
 echo "         checking for sched_setaffinity:    failed"
 echo "         checking for sched_getcpu():       failed"
fi

if [ -r "/usr/include/linux/getcpu.h" ];then 
 DEFINES="${DEFINES} -DGETCPU"
 echo "         checking for getcpu():             ok"
else
 echo "         checking for getcpu():             failed"
fi

gcc -o cpuinfo ${DEFINES} architecture.c x86.c generic.c -lm
