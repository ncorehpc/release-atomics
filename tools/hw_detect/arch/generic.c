/** B e n c h I T - Performance Measurement for Scientific Applications
* @file generic.c
*  generic implementations of the hardware detection using information provided by the opperating system
*  used for unsupported architekture, in case of errors in the architekture specific detection, and if
*  there is no architecture specific method to implement a function.
* 
* Author: Daniel Molka (daniel.molka@zih.tu-dresden.de)
*/
#include "cpu.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>

/* nedded to determine glibc version */
/* TODO check availability in MAC OS, AIX */
#define _GNU_SOURCE
#if (defined(linux) || defined(__linux__)) && defined(GLIBC_VERSION)
 #include <gnu/libc-version.h>
#endif

/* output for sh function used by some generic implementations */
char output[MAX_OUTPUT];
/* input for sh function used by some generic implementations */
char *argv[3];

#if (defined(linux) || defined(__linux__)) && defined (AFFINITY)
/* needed for restore_affinity function */
static cpu_set_t orig_cpuset;
static unsigned long long orig_cpu_mask;
static int restore=0;
#endif

/** 
  * get version of the glibc 
  */
#if defined(GLIBC_VERSION)
static double glibc_version()
{
  double res=0.0;
  char *version;
  const char *tmp;
  int i,j=0,dot=0,length;

  tmp=gnu_get_libc_version();
  length=strlen(tmp);
  version=malloc((length+1)*sizeof(char));

  for (i=0;i<length;i++)
  {
    if ((tmp[i]>='0')&&(tmp[i]<='9')) version[j++]=tmp[i];
    else if (tmp[i]=='.') {if (!dot) {dot=1;version[j++]=tmp[i];}}
    else {version[j]='\0';break;}
  }
  res = strtod(version,NULL);
  return res;
}
#else
 static double glibc_version()
{
  return 0.0;
}
#endif

/**
 * executes shellscripts
 */
int sh(char* script,int argc, char** args, char* output, int buffersize)
{
   int fd[2],status,i;
   pid_t pid;
   char *directory;
   char *path;
   char **sysfs_argv;
   char *tmp;
   int script_fd;

   directory=getenv("HW_DETECT_BASE");
   if (directory==NULL)
   {
      directory=getenv("BENCHITROOT");
      if (directory==NULL)
      {
         directory=getenv("PWD");
         if (directory==NULL) return -1;
         path=malloc((strlen(directory)+strlen(script)+6)*sizeof(char));
         sprintf(path,"%s/..%s",directory,script);
         script_fd=open(path,O_RDONLY);
         if (script_fd<0) return -1;
         close(script_fd);
      }
      else
      {
        path=malloc((strlen(directory)+strlen(script)+23)*sizeof(char));
        sprintf(path,"%s/tools/hw_detect%s",directory,script);
      }
   }
   else
   {
     path=malloc((strlen(directory)+strlen(script)+2)*sizeof(char));
     sprintf(path,"%s%s",directory,script);
   }
   memset(output,0,buffersize);
   sysfs_argv=malloc((argc+3)*sizeof(char*));
   sysfs_argv[0]=(char*) malloc(4*sizeof(char)); sprintf(sysfs_argv[0],"sh");
   sysfs_argv[1]=(char*) malloc((2+strlen(path))*sizeof(char)); sprintf(sysfs_argv[1],"%s",path);
   for (i=0;i<argc;i++)
   {
     sysfs_argv[i+2]=args[i];
   }
   sysfs_argv[i+2]=NULL;
   if (pipe(fd)==-1) return -1;
   pid=fork();
   if (pid==-1) return -1;
   if (pid==0) /* child */
   {
     /* attach pipe to stdout */
     close(fd[0]);
     dup2(fd[1],1);
     /* call cpuinfo.sh */
     tmp=&path[strlen(path)];
     while (*tmp!='/') tmp--;
     *tmp='\0';
     chdir(path);
     execvp("sh",sysfs_argv);
     exit(0);//in case of errors
   }
   if (pid>0) /* parent */
   {
     close(fd[1]);
     wait(&status);
     /* read cpuinfo.sh output from pipe */
     read(fd[0],output,MAX_OUTPUT-1);
     close(fd[0]);
   }
   tmp=strstr(output,"\n");
   while (tmp!=NULL)
   {
     *tmp='\0';
     tmp=strstr(output,"\n");
   }
   return 0;
}

/* 
 * architecture independent implementations 
 */

 /**
  * Determine number of CPUs in System
  */
int num_cpus()
{
   int num;
   num=sysconf(_SC_NPROCESSORS_CONF);
   if (num==-1)
   {
     /*TODO proc/cpuinfo*/
     argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"num_cpus");
     if (sh("/sysfs/cpuinfo.sh",1,argv,output,MAX_OUTPUT)==-1) num= -1;
     else if (!strcmp(output,"n/a")) num= -1;
     else if (!strcmp(output,"")) num= -1;
     else if (atoi(output)<=0) num= -1;
     else num=atoi(output);
   }
   /*assume 1 if detection fails*/
   if (num<1) num=1;
   return num;
}

 /**
  * try to estimate ISA using compiler macros
  */
void get_architecture(char* arch)
{
  #if ((defined (__i386__))||(defined (__i386))||(defined (i386)))
   strcpy(arch,"i386");
  #endif

  #if ((defined (__i486__))||(defined (__i486))||(defined (i486)))
   strcpy(arch,"i486");
  #endif

  #if ((defined (__i586__))||(defined (__i586))||(defined (i586)))
   strcpy(arch,"i586");
  #endif

  #if ((defined (__i686__))||(defined (__i686))||(defined (i686)))
   strcpy(arch,"i686");
  #endif

  #if ((defined (__x86_64__))||(defined (__x86_64))||(defined (x86_64)))
   strcpy(arch,"x86_64");
  #endif
}

/** 
 *sets affinity to a certain cpu 
 */
int set_cpu(int cpu)
{
#if (defined(linux) || defined(__linux__)) && defined (AFFINITY)
    cpu_set_t cpuset;
    unsigned long long mask;
    int err=-1,i;

    /* earlier versions did not provide the affinity functions 
       this is a bit overcautious but might prevent crashes when downgrading glibc after compilation */
    if (glibc_version()<2.34) return -1;

    /* test if the CPU is allowed to be used (sched_setaffinity otherwise would overwrite taskset settings) */
    err=sched_getaffinity(0, sizeof(cpu_set_t), &orig_cpuset);
    if ((!err)&&(!CPU_ISSET(cpu,&orig_cpuset))) return -2;

    CPU_ZERO(&cpuset);
    CPU_SET(cpu,&cpuset);
    err=sched_setaffinity(0, sizeof(cpu_set_t), &cpuset);
    if (!err) {restore=1;err=sched_getaffinity(0, sizeof(cpu_set_t), &cpuset);}

    if (!CPU_ISSET(cpu,&cpuset)) err=-1;
    for (i=0;i<num_cpus();i++) if ((i!=cpu)&&(CPU_ISSET(i,&cpuset))) err=-1;

    /* if affinity functions are not available and in case of an error try fallback implementation without macros */
    if(err)
    {
      /* test if the CPU is allowed to be used (sched_setaffinity otherwise would overwrite taskset settings) */
      err=sched_getaffinity(0, sizeof(unsigned long long), (cpu_set_t*) &orig_cpu_mask);
      printf("%8llux\n",mask);
      if ((!err)&&(!(orig_cpu_mask&(1<<cpu)))) return -2;

      mask=(1<<cpu);
      err=sched_setaffinity(0, sizeof(unsigned long long), (cpu_set_t*) &mask);
      if (!err) {restore=2;err=sched_getaffinity(0, sizeof(unsigned long long), (cpu_set_t*) &mask);}
      if (mask!=(1<<cpu)) err=-1;
    }

    return err;
#else
  return -1;
#endif
}

/** 
 * restores original affinity after changing with set_cpu() 
 */
int restore_affinity()
{
  #if (defined(linux) || defined(__linux__)) && defined (AFFINITY)
    if (restore==1) sched_setaffinity(0, sizeof(cpu_set_t), &orig_cpuset);
    if (restore==2) sched_setaffinity(0, sizeof(unsigned long long), (cpu_set_t*) &orig_cpu_mask);
    restore=0;
  #endif
  return 0;
}

/** 
 * tries to determine on which cpu the program is being run 
 */
int get_cpu()
{
  int cpu=-1;
  #if (defined(linux) || defined(__linux__)) && defined (GETCPU)
    int c,s;
    s=getcpu(&c,NULL,NULL);
    cpu= (s == -1) ? s : c;
  #else
    #if (defined(linux) || defined(__linux__)) && defined (SCHED_GETCPU)
      /* earlier versions did not provide sched_getcpu,
         this is a bit overcautious but might prevent crashes when downgrading glibc after compilation */
      if (glibc_version()<2.6) cpu = -1;
      else cpu = sched_getcpu();
    #endif
  #endif
  return cpu;
}

/** 
 * tries to determine the physical package, a cpu belongs to
 */
int get_pkg(int cpu)
{
  int pkg;
  char *buffer;

  if ((num_cpus()==1)||(num_packages()==1)) return 0;

  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(10*sizeof(char));
     buffer=malloc(10*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"package");
     if (sh("/sysfs/cpuinfo.sh",2,argv,buffer,10)) pkg = -1;
     else if (!strcmp(buffer,"n/a")) pkg= -1;
     else pkg=atoi(buffer);

     if (pkg==-1)
     {
       /* if the number of cpus equals the number of packages assume pkg_id = cpu_id*/
       if (num_cpus()==num_packages()) pkg = cpu;
       /* if there is only one core per package assume pkg_id = core_id */
       else if (num_cores_per_package()==1) pkg = get_core_id(cpu);
       /* if the number of packages equals the number of numa nodes assume pkg_id = numa node */
       else if (num_numa_nodes()==num_packages()) pkg = get_numa_node(cpu);

       /* NOTE pkg_id in UMA Systems with multiple sockets and more than 1 Core per socket can't be determined
               without correct topology information in sysfs*/
     }
  }

  return pkg;
}

/** 
 * tries to determine the core ID, a cpu belongs to
 */
int get_core_id(int cpu)
{
  int core=-1;
  char *buffer;

  if (num_cpus()==1) return 0;

  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(10*sizeof(char));
     buffer=malloc(10*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"core-id");
     if (sh("/sysfs/cpuinfo.sh",2,argv,buffer,10)) core = -1;
     else if (!strcmp(buffer,"n/a")) core= -1;
     else core=atoi(buffer);
  }
  if (core==-1)
  {
    /* if each package contains only one cpu assume core_id = package_id = cpu_id */
    if (num_cores_per_package()==1) core = 0;

    /* NOTE core_id can't be determined without correct topology information in sysfs if there are multiple cores per package 
       TODO /proc/cpuinfo */
  }
  return core;
}

/**
 * determines how many NUMA Nodes are in the system
 */
int num_numa_nodes()
{
   argv[0]=(char*)malloc(18*sizeof(char));sprintf(argv[0],"num_numa_nodes");
   if (sh("/sysfs/cpuinfo.sh",1,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   if (atoi(output)<=0) return -1;

   return atoi(output);
}

/** 
 * tries to determine the NUMA Node, a cpu belongs to
 */
int get_numa_node(int cpu)
{
  int node=-1;
  char *buffer;

  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(10*sizeof(char));
     buffer=malloc(10*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"numa_node");
     if (sh("/sysfs/cpuinfo.sh",2,argv,buffer,10)) node = -1;
     else if (!strcmp(buffer,"n/a")) node= -1;
     else node=atoi(buffer);
  }

  return node;
}

 /**
  * frequency scaling
  */
 int supported_frequencies(int cpu, char* output)
 {
  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(14*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"frequencies");
     if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT))return -1;
     if(!strcmp(output,"n/a"))return -1;
     return 0;
  }
  else return -1;
 }
 extern int scaling_governor(int cpu, char* output)
 {
  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(20*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"scaling_governor");
     if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT))return -1;
     if(!strcmp(output,"n/a"))return -1;
     return 0;
  }
  else return -1;
 }
 extern int scaling_driver(int cpu, char* output)
 {
  if (cpu==-1) cpu=get_cpu();
  if (cpu!=-1)
  {
     argv[0]=malloc(10*sizeof(char));
     argv[1]=malloc(10*sizeof(char));

     sprintf(argv[0],"cpu%i",cpu);
     sprintf(argv[1],"scaling_driver");
     if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT))return -1;
     if(!strcmp(output,"n/a"))return -1;
     return 0;
  }
  else return -1;
 }

 /*
  * generic implementations for architecture dependent functions
  */

 /**
  * basic information about cpus
  */
 int generic_get_cpu_vendor(char* vendor){return -1;}            /*TODO /proc/cpuinfo */
 int generic_get_cpu_name(char* name){return -1;}                /*TODO /proc/cpuinfo */
 int generic_get_cpu_codename(char* name){return -1;}            /*TODO /proc/cpuinfo -> generic_properties.list (MIPS,SPARC)*/
 int generic_get_cpu_family(){return -1;}                        /*TODO /proc/cpuinfo */
 int generic_get_cpu_model(){return -1;}                         /*TODO /proc/cpuinfo */
 int generic_get_cpu_stepping(){return -1;}                      /*TODO /proc/cpuinfo */
 int generic_get_cpu_gate_length(){return -1;}                   /*TODO /proc/cpuinfo -> generic_properties.list (MIPS,SPARC)*/

 /**
  * additional features (e.g. SSE)
  */
 int generic_get_cpu_isa_extensions(char* features){return -1;}  /*TODO /proc/cpuinfo */

 /**
  * read clockrate from sysfs
  * @param check ignored
  * @param cpu used to find accosiated directory in sysfs
  */
 unsigned long long generic_get_cpu_clockrate(int check, int cpu)
 {
   char tmp[MAX_OUTPUT];
 
   if (cpu==-1) cpu=get_cpu();
   if (cpu==-1) return -1;

   memset(tmp,0,MAX_OUTPUT*sizeof(char));scaling_governor(cpu, tmp);

   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cpu%i",cpu);
   argv[1]=(char*)malloc(14*sizeof(char));
   if ((!strcmp(tmp,"performance"))||(!strcmp(tmp,"powersave"))) sprintf(argv[1],"cur_freq"); 
   else sprintf(argv[1],"max_freq");
   
   if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT)==-1) return 0;
   if (!strcmp(output,"n/a")) return 0;
   else return (unsigned long long) atoll(output);
 }

 /**
  * returns a timestamp from cpu-internal counters (if available)
  */
 unsigned long long generic_timestamp()
 {
   struct timeval tv;

   if (gettimeofday(&tv,NULL)==0) return ((unsigned long long)tv.tv_sec)*1000000+tv.tv_usec;
   else return 0;
 }

 /**
  * number of caches (of one cpu)
  */
 int generic_num_caches(int cpu)
 {
   if (cpu==-1) cpu=get_cpu();
   if (cpu==-1) return -1;

   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cpu%i",cpu);
   argv[1]=(char*)malloc(14*sizeof(char));sprintf(argv[1],"num_caches");
   if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   if (atoi(output)<=0) return -1;
   return atoi(output);
 }

 /**
  * information about the cache: level, associativity...
  */
 int generic_cache_info(int cpu,int id, char* output)
 {
   if (cpu==-1) cpu=get_cpu();
   if (cpu==-1) return -1;

   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cpu%i",cpu);
   argv[1]=(char*)malloc(14*sizeof(char));sprintf(argv[1],"cache_info");
   argv[2]=(char*)malloc(14*sizeof(char));sprintf(argv[2],"%i",id);
   if (sh("/sysfs/cpuinfo.sh",3,argv,output,MAX_OUTPUT)==-1) return -1;
   return 0;
 }

 /**
  * number of tlbs (of one cpu)
  */
 extern int generic_num_tlbs(int cpu){return -1;} /*TODO /proc/cpuinfo */
 /**
  * information about the tlb: level, number of entries...
  */
 extern int generic_tlb_info(int cpu, int id, char* output){return -1;}/* TODO /proc/cpuinfo */

  /**
  * the following four functions describe how the CPUs are distributed among packages
  * num_cpus() = num_packages() * num_threads_per_package()
  * num_threads_per_package() = num_cores_per_package() * num_threads_per_core()
  */
 int generic_num_packages()
 {
   /*TODO proc/cpuinfo*/
   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"num_packages");
   if (sh("/sysfs/cpuinfo.sh",1,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   if (atoi(output)<=0) return -1;
   return atoi(output);
 }
 int generic_num_cores_per_package()
 {
   char* tmp;
   int num=0;

   /*TODO proc/cpuinfo*/
   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cores_in_pkg");
   argv[1]=(char*)malloc(14*sizeof(char));sprintf(argv[1],"0");
   if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   tmp=output;
   do
   {
    tmp=strstr(tmp,"core");
    if (tmp!=NULL) {tmp++;num++;}
   }
   while (tmp!=NULL);

   if (num==0) return -1;
   return num;
 }

 int generic_num_threads_per_core()
 {
   char* tmp;
   int num=0;

   /*TODO proc/cpuinfo*/
   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cpus_in_core");
   argv[1]=(char*)malloc(14*sizeof(char));sprintf(argv[1],"0");
   if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   tmp=output;
   do
   {
    tmp=strstr(tmp,"cpu");
    if (tmp!=NULL) {tmp++;num++;}
   }
   while (tmp!=NULL);

   if (num==0) num=generic_num_threads_per_package()/generic_num_cores_per_package();
   if (num!=generic_num_threads_per_package()/generic_num_cores_per_package()) return -1;
   return num;
 }

 int generic_num_threads_per_package()
 {
   char* tmp;
   int num=0;

   /*TODO proc/cpuinfo*/
   argv[0]=(char*)malloc(14*sizeof(char));sprintf(argv[0],"cpus_in_pkg");
   argv[1]=(char*)malloc(14*sizeof(char));sprintf(argv[1],"0");
   if (sh("/sysfs/cpuinfo.sh",2,argv,output,MAX_OUTPUT)==-1) return -1;
   if (!strcmp(output,"n/a")) return -1;
   if (!strcmp(output,"")) return -1;
   tmp=output;
   do
   {
    tmp=strstr(tmp,"cpu");
    if (tmp!=NULL) {tmp++;num++;}
   }
   while (tmp!=NULL);

   if (num==0) return -1;
   return num;
 }

 /**
  * paging related information
  */
 int generic_get_virt_address_length(){return -1;}
 int generic_get_phys_address_length(){return -1;}
 int generic_num_pagesizes(){return -1;}
 long long generic_pagesize(int id){return -1;}

/* see cpu.h */
#if defined (__ARCH_UNKNOWN)

 /*
  * use generic implementations for unknown architectures
  */

 int get_cpu_vendor(char* vendor){return generic_get_cpu_vendor(vendor);}
 int get_cpu_name(char* name){return generic_get_cpu_name(name);}
 int get_cpu_codename(char* name){return generic_get_cpu_codename(name);}
 int get_cpu_family(){return generic_get_cpu_family();}
 int get_cpu_model(){return generic_get_cpu_model();}
 int get_cpu_stepping(){return generic_get_cpu_stepping();}
 int get_cpu_gate_length(){return generic_get_cpu_gate_length();}
 int get_cpu_isa_extensions(char* features) {return generic_get_cpu_isa_extensions(features);}
 unsigned long long get_cpu_clockrate(int check, int cpu){return generic_get_cpu_clockrate(check,cpu);}
 unsigned long long timestamp(){return generic_timestamp();}
 int num_caches(int cpu) {return generic_num_caches(cpu);} 
 int cache_info(int cpu,int id, char* output) {return generic_cache_info(cpu,id,output);}
 int num_tlbs(int cpu) {return generic_num_tlbs(cpu);}
 int tlb_info(int cpu, int id, char* output) {return generic_tlb_info(cpu,id,output);}
 int num_packages(){return generic_num_packages();}
 int num_cores_per_package(){return generic_num_cores_per_package();}
 int num_threads_per_core(){return generic_num_threads_per_core();}
 int num_threads_per_package(){return generic_num_threads_per_package();}
 int get_virt_address_length(){return generic_get_virt_address_length();}
 int get_phys_address_length(){return generic_get_phys_address_length();}
 int num_pagesizes(){return generic_num_pagesizes();}
 long long pagesize(int id){return generic_pagesize(id);}

#endif

