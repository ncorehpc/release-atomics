/** B e n c h I T - Performance Measurement for Scientific Applications
* @file sysconf.c
* gathers information using sysconf(), which is available on all POSIX compliant systems
*
* Author: Daniel Molka (Daniel.Molka@zih.tu-dresden.de)
* Last change by: $Author: molka $
* $Revision: 1.1 $
* $Date: 2008/07/31 06:54:02 $
*/
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[])
{

  if (argc>2) {printf("sysconf: illegal parameter\n"); exit(-1);}

  if((argc==1)||(!strcmp(argv[1],"test")))
  {
    printf("_SC_PHYS_PAGES:        %12li\n",sysconf(_SC_PHYS_PAGES));
    printf("_SC_AVPHYS_PAGES:      %12li\n",sysconf(_SC_AVPHYS_PAGES));
    printf("_SC_PAGESIZE:          %12li\n",sysconf(_SC_PAGESIZE));
    printf("_SC_NPROCESSORS_CONF:  %12li\n",sysconf(_SC_NPROCESSORS_CONF));
    printf("_SC_NPROCESSORS_ONLN:  %12li\n",sysconf(_SC_NPROCESSORS_ONLN));
    return 0;
  }

  if (argc==2)
  {
    if(!strcmp(argv[1],"memsize"))
      { 
         if ((sysconf(_SC_PHYS_PAGES)==-1)||(sysconf(_SC_PAGESIZE)==-1)) {printf("sysconf: internal error\n"); exit(-1);}
         printf("%llu\n",(unsigned long long)sysconf(_SC_PHYS_PAGES)*sysconf(_SC_PAGESIZE));
         return 0;
      }

    if(!strcmp(argv[1],"pagesize"))
      { 
         if (sysconf(_SC_PAGESIZE)==-1) {printf("sysconf: internal error\n"); exit(-1);}
         printf("%li\n",sysconf(_SC_PAGESIZE));
         return 0;
      }

    if(!strcmp(argv[1],"num_cpus"))
      { 
         if (sysconf(_SC_NPROCESSORS_CONF)==-1){printf("sysconf: internal error\n"); exit(-1);}
         printf("%li\n",sysconf(_SC_NPROCESSORS_CONF));
         return 0;
      }
  }

  printf("sysconf: illegal parameter\n");
  exit(-1);
}
/*****************************************************************************
Log-History

$Log: sysconf.c,v $
Revision 1.1  2008/07/31 06:54:02  molka
added experimental hardware detection

*****************************************************************************/
