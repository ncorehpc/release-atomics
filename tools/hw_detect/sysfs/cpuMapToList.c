#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  int pos=0;
  char *current;
  int cur_hex;

  if (argc!=2) return -1;
  while (strlen(argv[1]))
  {
    current=&(argv[1][strlen(argv[1])-1]);
    if (*current!=',')
    {
      cur_hex=(int)strtol(current,NULL,16);
      if (cur_hex&0x1) printf("cpu%i ",pos);
      if (cur_hex&0x2) printf("cpu%i ",pos+1);
      if (cur_hex&0x4) printf("cpu%i ",pos+2);
      if (cur_hex&0x8) printf("cpu%i ",pos+3);
      pos+=4;
    }
    *current='\0';
  }
  printf("\n");
  
  return 0;
}
