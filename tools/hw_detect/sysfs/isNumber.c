#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char** argv)
{
  char* error;
  long tmp;

  if (argc==2) tmp=strtol(argv[1],&error,10);

  if ((argc==2)&&(strlen(error)==0)) printf("yes\n");
  else printf("no\n");

  return 0;
}