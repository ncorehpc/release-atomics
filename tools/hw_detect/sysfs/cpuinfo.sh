OLD_PWD=${PWD};
if [ "${HW_DETECT_BASE}" != "" ];then cd "${HW_DETECT_BASE}/sysfs";fi
if [ "${BENCHITROOT}" != "" ];then cd "${BENCHITROOT}/tools/hw_detect/sysfs";fi
if [ -d "${PWD}/../sysfs" ];then cd "${PWD}/../sysfs"; fi

if [ "$2" = "max_freq"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_max_freq ]; then
  echo "`cat /sys/devices/system/cpu/$1/cpufreq/scaling_max_freq` *1000" | bc -s
 else
  if [ -r /sys/devices/system/cpu/$1/cpufreq/cpuinfo_max_freq ]; then
   echo "`cat /sys/devices/system/cpu/$1/cpufreq/cpuinfo_max_freq` *1000" | bc -s
  else
   echo "n/a"
  fi
 fi
fi

if [ "$2" = "min_freq"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_min_freq ]; then
  echo "`cat /sys/devices/system/cpu/$1/cpufreq/scaling_min_freq` *1000" | bc -s
 else
    if [ -r /sys/devices/system/cpu/$1/cpufreq/cpuinfo_min_freq ]; then
   echo "`cat /sys/devices/system/cpu/$1/cpufreq/cpuinfo_min_freq` *1000" | bc -s
  else
   echo "n/a"
  fi
 fi
fi

if [ "$2" = "cur_freq"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_cur_freq ]; then
  echo "`cat /sys/devices/system/cpu/$1/cpufreq/scaling_cur_freq` *1000" | bc -s
 else
    if [ -r /sys/devices/system/cpu/$1/cpufreq/cpuinfo_cur_freq ]; then
   echo "`cat /sys/devices/system/cpu/$1/cpufreq/cpuinfo_cur_freq` *1000" | bc -s
  else
   echo "n/a"
  fi
 fi
fi

if [ "$2" = "frequencies"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_available_frequencies ]; then
  OUTPUT=""
  TEMP="`cat /sys/devices/system/cpu/$1/cpufreq/scaling_available_frequencies`"
  for i in ${TEMP}; do
    OUTPUT="`echo "${i} /1000 " | bc -s`MHz ${OUTPUT}"
  done
 else
   OUTPUT=""
   if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_min_freq ]; then
    FREQ="`cat /sys/devices/system/cpu/$1/cpufreq/scaling_min_freq`"
    OUTPUT="${OUPUT} `echo "${FREQ} /1000 " | bc -s`MHz "
   fi
   if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_max_freq ]; then
    FREQ="`cat /sys/devices/system/cpu/$1/cpufreq/scaling_max_freq`"
    OUTPUT="${OUTPUT} `echo "${FREQ} /1000 " | bc -s`MHz "
   fi
   if [ "${OUTPUT}" = "" ]; then OUTPUT="n/a";fi
 fi
 echo ${OUTPUT}
fi

if [ "$2" = "scaling_governor"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_governor ]; then
  echo "`cat /sys/devices/system/cpu/$1/cpufreq/scaling_governor`" 
 else
  echo "n/a"
 fi
fi

if [ "$2" = "scaling_driver"  ]; then
 if [ -r /sys/devices/system/cpu/$1/cpufreq/scaling_driver ]; then
  echo "`cat /sys/devices/system/cpu/$1/cpufreq/scaling_driver`" 
 else
  echo "n/a"
 fi
fi

if [ "$1" = "num_packages"  ]; then
  if [ -d /sys/devices/system/cpu ]; then
   LIST=""
   for i in /sys/devices/system/cpu/cpu*
   do
     if [ -r $i/topology/physical_package_id ]; then
      EXISTS="0"
      for j in ${LIST}
      do
       if [ "$j" = "`cat $i/topology/physical_package_id`" ];then
         EXISTS="1"
       fi
      done
      if [ "${EXISTS}" = "0" ]; then
       LIST="${LIST} `cat $i/topology/physical_package_id`"
      fi
     fi
   done
   NUM="0"
   for i in ${LIST}
   do
    NUM="`echo \"${NUM} +1\" | bc -s `"
   done
   if [ "${NUM}" != "0" ];then
    echo "${NUM}"
   else
    echo "n/a"
   fi
  else
   echo "n/a"
  fi
fi

if [ "$2" = "package"  ]; then
 if [ -r /sys/devices/system/cpu/$1/topology/physical_package_id ]; then
  echo "`cat /sys/devices/system/cpu/$1/topology/physical_package_id`"
 else
  echo "n/a"
 fi
fi

if [ "$1" = "num_numa_nodes"  ]; then
 if [ -d /sys/devices/system/node ]; then
  if [ ! -x isNumber ] && [ -r isNumber.c ]; then
    gcc -o isNumber isNumber.c
  fi
  if [ -x isNumber ];then
    NUM="0"
    for i in /sys/devices/system/node/node*
    do
      ID="`echo $i | cut -b30-`"
      if [ "`./isNumber ${ID}`" = "yes" ]; then
        NUM="`echo \"${NUM} +1\" | bc -s `"
      fi
    done
    if [ "${NUM}" != "0" ];then
     echo "${NUM}"
    else
     echo "n/a"
    fi
  else
   echo "n/a"
  fi
 else
  echo "n/a"
 fi
fi

if [ "$2" = "numa_node"  ]; then
 if [ -d /sys/devices/system/node ] && [ -d /sys/devices/system/cpu/$1 ]; then
  for i in /sys/devices/system/node/node*
  do
    if [ -d $i/$1 ]; then
      echo "`echo \"$i\" | cut -b30-`"
    fi
  done
 else
  echo "n/a"
 fi
fi


if [ "$2" = "core-id"  ]; then
 if [ -r /sys/devices/system/cpu/$1/topology/core_id ]; then
  echo "`cat /sys/devices/system/cpu/$1/topology/core_id`"
 else
  echo "n/a"
 fi
fi

if [ "$1" = "num_cpus"  ]; then
 if [ -d /sys/devices/system/cpu ]; then
  if [ ! -x isNumber ] && [ -r isNumber.c ]; then
    gcc -o isNumber isNumber.c
  fi
  if [ -x isNumber ];then
    NUM="0"
    for i in /sys/devices/system/cpu/cpu*
    do
      ID="`echo $i | cut -b28-`"
      if [ "`./isNumber ${ID}`" = "yes" ]; then
        NUM="`echo \"${NUM} +1\" | bc -s `"
      fi
    done
    echo "${NUM}"
  else
   echo "n/a"
  fi
 else
  echo "n/a"
 fi
fi

if [ "$1" = "cpus_in_pkg"  ]; then
 if [ -d /sys/devices/system/cpu ]; then
  OUTPUT=""
  for i in /sys/devices/system/cpu/cpu*
  do
    if [ -r $i/topology/physical_package_id ] && [ "`cat $i/topology/physical_package_id`" = "$2" ] ; then
     OUTPUT="$OUTPUT`echo $i | cut -b25-` "
    fi
  done
  if [ "${OUTPUT}" = "" ]; then
   echo "n/a"
  else
   echo "${OUTPUT}"
  fi
 else
  echo "n/a"
 fi
fi

if [ "$1" = "cpus_in_core"  ]; then
 if [ -d /sys/devices/system/cpu ]; then
  OUTPUT=""
  for i in /sys/devices/system/cpu/cpu*
  do
    if [ -r $i/topology/core_id ] && [ "`cat $i/topology/core_id`" = "$2" ] ; then
     OUTPUT="$OUTPUT`echo $i | cut -b25-` "
    fi
  done
  if [ "${OUTPUT}" = "" ]; then
   echo "n/a"
  else
   echo "${OUTPUT}"
  fi
 else
  echo "n/a"
 fi
fi

if [ "$1" = "cores_in_pkg"  ]; then
 if [ -d /sys/devices/system/cpu ]; then
  OUTPUT=""
  for i in /sys/devices/system/cpu/cpu*
  do
    if [ -r $i/topology/physical_package_id ] && [ "`cat $i/topology/physical_package_id`" = "$2" ] ; then
     if [ -r $i/topology/core_id ]; then
      EXISTS="0"
      for j in ${OUTPUT}
      do
       if [ "$j" = "core`cat $i/topology/core_id`" ];then
         EXISTS="1"
       fi
      done
      if [ "${EXISTS}" = "0" ]; then
       OUTPUT="${OUTPUT}core`cat $i/topology/core_id` "
      fi
     fi
    fi
  done
  if [ "${OUTPUT}" = "" ]; then
   echo "n/a"
  else
   echo "${OUTPUT}"
  fi
 else
  echo "n/a"
 fi
fi

if [ "$2" = "num_caches"  ]; then
 if [ -d /sys/devices/system/cpu/$1/cache ]; then
  NUM="0"
  for i in /sys/devices/system/cpu/$1/cache/index*
  do
      NUM="`echo \"${NUM} +1\" | bc -s `"
  done
  echo "${NUM}"
 else
  echo "n/a"
 fi
fi

if [ "$2" = "cache_info"  ]; then
 DIR="/sys/devices/system/cpu/$1/cache/index$3"
 OUTPUT=""
 if [ -d ${DIR} ]; then
   if [ -r ${DIR}/level ];then 
     OUTPUT="${OUTPUT}Level `cat \"${DIR}/level\"`"
   fi
   if [ -r ${DIR}/type ];then 
     if [ "`cat \"${DIR}/type\"`" = "Unified" ];then
      OUTPUT="`cat \"${DIR}/type\"` ${OUTPUT}"
     else
      OUTPUT="${OUTPUT} `cat \"${DIR}/type\"`"
     fi
   fi
   OUTPUT="${OUTPUT} Cache:"
   if [ -r ${DIR}/size ];then 
     OUTPUT="${OUTPUT} `cat \"${DIR}/size\"`"
   fi
   if [ -r ${DIR}/ways_of_associativity ];then 
     OUTPUT="${OUTPUT}, `cat \"${DIR}/ways_of_associativity\"`-way set associative"
   fi
   if [ -r ${DIR}/coherency_line_size ];then 
     OUTPUT="${OUTPUT}, `cat \"${DIR}/coherency_line_size\"` Byte cachelines"
   fi
   if [ -r ${DIR}/shared_cpu_map ];then
     if [ ! -x cpuMapToList ] && [ -r cpuMapToList.c ]; then
        gcc -o cpuMapToList cpuMapToList.c
     fi
     if [ -x cpuMapToList ];then
       CPU_MAP="`cat \"${DIR}/shared_cpu_map\"`"
       CPU_LIST="`./cpuMapToList ${CPU_MAP}`"
       if [ "$CPU_LIST" = "$1 " ]; then
         OUTPUT="${OUTPUT}, exclusiv for $1"
       else
         OUTPUT="${OUTPUT}, shared among"
         for i in ${CPU_LIST}
         do
           OUTPUT="${OUTPUT} $i"
         done
       fi
     fi
   fi
   echo "${OUTPUT}"
 else
  echo "n/a"
 fi
fi

cd "${OLD_PWD}"
