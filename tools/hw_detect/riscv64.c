/**
* @file x86.c
*  architecture specific part of the hardware detection for x86 architectures
*  Uses CPUID and RDTSC instructions if available
* currently only AMD and Intel CPUs are supported according to their CPUID specifications
* TODO other vendors
*
* Author: Daniel Molka (daniel.molka@zih.tu-dresden.de)
*/
#include "cpu.h"
#include "riscv64.h"
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "properties.h"

/** used to store Registers {R|E}AX, {R|E}BX, {R|E}CX and {R|E}DX */
static unsigned long long a,b,c,d;

int get_virt_address_length()
{
  return 48;
}

int get_phys_address_length()
{
  return 36;
}

int num_pagesizes()
{
  return 0;
}

long long pagesize(int id)
{
  if (id>num_pagesizes()) return -1;

  if (id==0) return 4096;
  if (id==1) return 2097152;
  if (id==2) return 1073741824;

  return -1;
}


/**
 * shared implementations for 32 Bit and 64 Bit mode
 */

 /**
  * try to estimate ISA using compiler macros
  */
void get_architecture(char* arch, size_t len)
{
   strncpy(arch,"X280",len);
}


static int has_htt()
{
 return 0;
}

int get_cpu_vendor(char* vendor, size_t len)
{
  char tmp_vendor[13];
  tmp_vendor[12]='\0';
  strcpy(tmp_vendor,"SiFive");
  strncpy(vendor,tmp_vendor,sizeof(tmp_vendor));
  return 0;
}

int get_cpu_name(char* name, size_t len)
{
  char vendor[13];
  strcpy(vendor,"SiFive");
  strncpy(name,vendor,sizeof(vendor));
  return 0;

  return generic_get_cpu_name(name,len);
}

int get_cpu_family()
{
      return generic_get_cpu_family();
}

int get_cpu_model()
{
      return generic_get_cpu_model();
}

int get_cpu_stepping()
{
      return generic_get_cpu_stepping();
}

int get_cpu_isa_extensions(char* features, size_t len)
{
   return generic_get_cpu_isa_extensions(features,len);
}

/**
 * measures clockrate using the Time-Stamp-Counter
 * @param check if set to 1 only constant TSCs will be used (i.e. power management independent TSCs)
 *              if set to 0 non constant TSCs are allowed (e.g. AMD K8)
 * @param cpu the cpu that should be used, only relevant for the fallback to generic functions
 *            if TSC is available and check is passed or deactivated then it is assumed thet the affinity
 *            has already being set to the desired cpu
 * @return frequency in highest P-State, 0 if no invariant TSC is available
 */
unsigned long long get_cpu_clockrate(int check,int cpu)
{
   unsigned long long start1_tsc,start2_tsc,end1_tsc,end2_tsc;
   unsigned long long start_time,end_time;
   unsigned long long clock_lower_bound,clock_upper_bound,clock;
   unsigned long long clockrate=0;
   int i,num_measurements=0,min_measurements;
   char tmp[_HW_DETECT_MAX_OUTPUT];
   struct timeval ts;

   if (check)
   {
     /* non invariant TSCs can be used if CPUs run at fixed frequency */
     scaling_governor(-1, tmp, _HW_DETECT_MAX_OUTPUT);
     return generic_get_cpu_clockrate(check,cpu);
     min_measurements=5;
   }
   else min_measurements=20;

   return generic_get_cpu_clockrate(check,cpu);

   i=3;
   do
   {
      //start timestamp
      start1_tsc=timestamp();
      gettimeofday(&ts,NULL);
      start2_tsc=timestamp();

      start_time=ts.tv_sec*1000000+ts.tv_usec;

      //waiting
      if (check) usleep(1000*i);    /* sleep */
      else do {end1_tsc=timestamp();} while (end1_tsc<start2_tsc+1000000*i); /* busy waiting */

      //end timestamp
      end1_tsc=timestamp();
      gettimeofday(&ts,NULL);
      end2_tsc=timestamp();

      end_time=ts.tv_sec*1000000+ts.tv_usec;

      clock_lower_bound=(((end1_tsc-start2_tsc)*1000000)/(end_time-start_time));
      clock_upper_bound=(((end2_tsc-start1_tsc)*1000000)/(end_time-start_time));

      // if both values differ significantly, the measurement could have been interrupted between 2 rdtsc's
      if (((double)clock_lower_bound>(((double)clock_upper_bound)*0.999))&&((end_time-start_time)>2000))
      {
        num_measurements++;
        clock=(clock_lower_bound+clock_upper_bound)/2;
        if(clockrate==0) clockrate=clock;
        else if ((check)&&(clock<clockrate)) clockrate=clock;
        else if ((!check)&&(clock>clockrate)) clockrate=clock;
      }
      i+=2;
    }
    while (((end_time-start_time)<10000)||(num_measurements<min_measurements));

   return clockrate;
}
/**
 * number of caches (of one cpu)
 * @param cpu the cpu that should be used, only relevant for the fallback to generic functions
 *            if cpuid is available it is assumed that the affinity has already been set to the desired cpu
 */
int num_caches(int cpu)
{
return generic_num_caches(cpu);
}

/**
 * information about the cache: level, associativity...
 * @param cpu the cpu that should be used, only relevant for the fallback to generic functions
 *            if cpuid is available it is assumed that the affinity has already been set to the desired cpu
 * @param id id of the cache 0 <= id <= num_caches()-1
 * @param output preallocated buffer for the result string
 */
//TODO use sysfs if available to determine cache sharing
int cache_info(int cpu,int id, char* output, size_t len)
{
  return generic_cache_info(cpu,id,output,len);
}

 int cache_level(int cpu, int id) {
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,"Level");
   if (beg==NULL) return generic_cache_level(cpu,id);
   else beg+=6;
   end=strstr(beg," ");
   if (end!=NULL)*end='\0';

   return atoi(beg);
 }

 unsigned long long cache_size(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,",");
   if (beg==NULL) return generic_cache_size(cpu,id);
   else beg+=2;
   end=strstr(beg,",");
   if (end!=NULL) *end='\0';
   end=strstr(beg,"KiB");
   if (end!=NULL)
   {
     end--;
     *end='\0';
     return atoi(beg)*1024;
   }
   end=strstr(beg,"MiB");
   if (end!=NULL)
   {
     end--;
     *end='\0';
     return atoi(beg)*1024*1024;
   }

   return generic_cache_size(cpu,id);
 }
 unsigned int cache_assoc(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,",")+1;
   if (beg==NULL) return generic_cache_assoc(cpu,id);
   else beg++;
   end=strstr(beg,",")+1;
   if (end==NULL) return generic_cache_assoc(cpu,id);
   else end++;
   beg=end;
   end=strstr(beg,",");
   if (end!=NULL) *end='\0';
   end=strstr(tmp,"-way");
   if (end!=NULL) {
     *end='\0';
     return atoi(beg);
   }
   end=strstr(tmp,"fully");
   if (end!=NULL) {
     *end='\0';
     return FULLY_ASSOCIATIVE;
   }
   return generic_cache_assoc(cpu,id);
 }
 int cache_type(int cpu, int id) {
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=tmp;
   end=strstr(beg,",");
   if (end!=NULL)*end='\0';
   else return generic_cache_type(cpu,id);

   if (strstr(beg,"Unified")!=NULL) return UNIFIED_CACHE;
   if (strstr(beg,"Trace")!=NULL) return INSTRUCTION_TRACE_CACHE;
   if (strstr(beg,"Data")!=NULL) return DATA_CACHE;
   if (strstr(beg,"Instruction")!=NULL) return INSTRUCTION_CACHE;

   return generic_cache_type(cpu,id);
 }
 int cache_shared(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,"among");
   if (beg==NULL)
   {
     beg=strstr(tmp,"per cpu");
     if (beg!=NULL) return 1;
     else return generic_cache_shared(cpu,id);
   }
   beg+=6;
   end=strstr(beg,"cpus");
   if (end!=NULL)*(end--)='\0';

   return atoi(beg);
 }
 int cacheline_length(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   cache_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,",")+1;
   if (beg==NULL) return generic_cacheline_length(cpu,id);
   else beg++;
   end=strstr(beg,",")+1;
   if (end==NULL) return generic_cacheline_length(cpu,id);
   else end++;
   beg=end;
   end=strstr(beg,",")+1;
   if (end==NULL) return generic_cacheline_length(cpu,id);
   else end++;
   beg=end;
   end=strstr(beg,"Byte cachelines");
   if (end!=NULL) *(end--)='\0';

   return atoi(beg);
}

int num_tlbs(int cpu)
{
 return generic_num_tlbs(cpu);
}

int tlb_info(int cpu, int id, char* output,size_t len)
{
return generic_tlb_info(cpu,id,output,len);
}

 //TODO
 /* additional functions to query certain information about the TLB*/
 int tlb_level(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,"Level");
   if (beg==NULL) return generic_tlb_level(cpu,id);
   else beg+=6;
   end=strstr(beg," ");
   if (end!=NULL)*end='\0';

   return atoi(beg);
 }
 int tlb_entries(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,",");
   if (beg==NULL) return generic_tlb_entries(cpu,id);
   else beg+=2;
   end=strstr(beg,",");
   if (end!=NULL) *end='\0';
   end=strstr(beg,"entries");
   if (end!=NULL)
   {
     end--;
     *end='\0';
     return atoi(beg);
   }

   return generic_tlb_entries(cpu,id);
 }
 int tlb_assoc(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,",")+1;
   if (beg==NULL) return generic_tlb_assoc(cpu,id);
   else beg++;
   end=strstr(beg,",")+1;
   if (end==NULL) return generic_tlb_assoc(cpu,id);
   else end++;
   beg=end;
   end=strstr(beg,",");
   if (end!=NULL) *end='\0';
   end=strstr(tmp,"-way");
   if (end!=NULL) {
     *end='\0';
     return atoi(beg);
   }
   end=strstr(tmp,"fully");
   if (end!=NULL) {
     *end='\0';
     return FULLY_ASSOCIATIVE;
   }
   return generic_tlb_assoc(cpu,id);
 }
 int tlb_type(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=tmp;
   end=strstr(beg,",");
   if (end!=NULL)*end='\0';
   else return generic_tlb_type(cpu,id);

   if (strstr(beg,"Unified")!=NULL) return UNIFIED_TLB;
   if (strstr(beg,"Data")!=NULL) return DATA_TLB;
   if (strstr(beg,"Instruction")!=NULL) return INSTRUCTION_TLB;

   return generic_tlb_type(cpu,id);
 }
 int tlb_num_pagesizes(int cpu, int id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;
   int num=1;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,"for");
   if (beg==NULL) return generic_tlb_num_pagesizes(cpu,id);
   else beg+=4;
   end=strstr(beg,"pages");
   if (end!=NULL)*end='\0';
   else return generic_tlb_num_pagesizes(cpu,id);

   while (strstr(beg,",")!=NULL) {
     beg++;num++;
   }

   return num;
 }
 unsigned long long tlb_pagesize(int cpu, int id,int size_id){
   char tmp[_HW_DETECT_MAX_OUTPUT];
   char *beg,*end;
   int num=0;

   tlb_info(cpu,id,tmp,sizeof(tmp));
   beg=strstr(tmp,"for");
   if (beg==NULL) return generic_tlb_pagesize(cpu,id,size_id);
   else beg+=4;
   end=strstr(beg,"pages");
   if (end!=NULL){*end='\0';end--;}
   else return generic_tlb_pagesize(cpu,id,size_id);

   while (num!=size_id){
     if (strstr(beg,",")!=NULL) {beg++;num++;}
     else return generic_tlb_pagesize(cpu,id,size_id);
   }
   end=strstr(beg," ");
   if ((strstr(beg,",")!=NULL)&&(strstr(beg,",")<end)) end=strstr(beg,",");

   end=strstr(beg,"K");
   if (end!=NULL)
   {
     *end='\0';
     return atoi(beg)*1024;
   }
   end=strstr(beg,"M");
   if (end!=NULL)
   {
     *end='\0';
     return atoi(beg)*1024*1024;
   }
   end=strstr(beg,"G");
   if (end!=NULL)
   {
     *end='\0';
     return atoi(beg)*1024*1024*1024;
   }

   return num;
 }

int num_packages()
{
  if ((num_cpus()==-1)||(num_threads_per_package()==-1)) return generic_num_packages();
  else if (!has_htt()) return num_cpus();
  else return num_cpus()/num_threads_per_package();
}

int num_cores_per_package()
{
 return 8;
}

int num_threads_per_core()
{
  return num_threads_per_package()/num_cores_per_package();
}

int num_threads_per_package()
{
  return 1;
}

