(cd ./arch_x86_64/memory_bandwidth/
find . -name '*.gp' -exec sed -i '/set data style points/d' {} \;
#find . -name '*.gp' -exec sed -i 's/set yrange.*]/set yrange [0.000000e+00:*]/' {} \;
find . -name '*.gp' -execdir gnuplot {} \;)

(cd ./arch_x86_64/memory_latency/
find . -name '*.gp' -exec sed -i '/set data style points/d' {} \;
find . -name '*.gp' -exec sed -i '/set ytics/d' {} \;
find . -name '*.gp' -exec sed -i 's/set yrange.*]/set yrange [0.000000e+00:*]/' {} \;
find . -name '*.gp' -exec sed -i 's/".*1:2.*(CPU cycles)., //' {} \;
find . -name '*.gp' -exec sed -i 's/".*1:3.*(CPU cycles)., //' {} \;
find . -name '*.gp' -exec sed -i 's/".*1:4.*(CPU cycles)., //' {} \;
#find . -name '*.gp' -exec sed -i 's/p.nanoseconds.ot/plot/' {} \;
#find . -name '*.gp' -exec sed -i 's/.nanoseconds./s/' {} \;
find . -name '*.gp' -exec sed -i 's/cycles/nanosecs/' {} \;

find . -name '*.gp' -execdir gnuplot {} \;)
