#!/bin/bash

#cd arch_x86_64/
rm allPlots.tex

#\usepackage{times}
#\usepackage{uist}
#\usepackage{graphicx}
#\usepackage{comment}
#\usepackage{url}
#\urlstyle{same}

#packeges and begining
printf "\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{geometry}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\\\\geometry{a4paper, top=10mm, left=10mm, right=10mm, bottom=25mm}" >> allPlots.tex

printf "\\\\begin{document}" >> allPlots.tex

#frontpage
printf "\\\title{Results of all Benchmarks}
\\\author{Ian Lintault}
\maketitle
\\\tableofcontents" >> allPlots.tex

#\clearpage" >> allPlots.tex


rm -f foldernames*
find ../arch_x86_64/memory_latency/C/pthread/0/* -maxdepth 0 -exec readlink -f {} >> foldernames \;

printf "\n \clearpage \section{Latency}\n" >> allPlots.tex
while read p; do
	rm -f filenames #reset filenames
	rm -f temp
	find $p -name '*.bit' -exec readlink -f {} >> filenames \;
	i=0
	rm -f temp2
	while read bit; do
		if [ $i -eq 0 ] # If first file in folder
		then
			#Emmit Title and beginfigure
			sed -n '/kernelstring=/,/date=/p' $bit >> temp
			sed -i 's/kernelstring="arch_x86_64.memory_latency.C.pthread.0.//g' temp
			sed -i 's/date=//g' temp
			#printf $(cat temp)
			printf "\n\subsection{" >>allPlots.tex
			printf $(cat temp) >> allPlots.tex
			printf "}\n" >> allPlots.tex
			printf "\n \\\\begin{figure}[!htbp]" >> allPlots.tex

		fi
		#printf $bit

		#emmit graphic
		printf "\includegraphics[scale = 0.7]{{" >> allPlots.tex
		printf $bit >> allPlots.tex
		printf ".gp}.eps} ">> allPlots.tex

		#build comment
		printf "Plot " >> temp2
		printf $i" :" >> temp2
		sed -n '/BENCHIT_KERNEL_COMMENT=" 0B missaligned, alloc: L, hugep.: 1, use:/,/TLB:/p' $bit >> temp2


		i=$((i+1))
	done <filenames
		printf "\\\\end{figure}\n" >> allPlots.tex
		sed -i 's/BENCHIT_KERNEL_COMMENT=" 0B missaligned, alloc: L, hugep.: 1, use://g' temp2
		sed -i 's/ - E2, TLB: 0"//g' temp2
		sed -i 's/, TLB: 0"//g' temp2
		sed -i 's/\n//g' temp2

		#emmit comment
		cat temp2 >> allPlots.tex

	printf "\clearpage" >>allPlots.tex # finished one folder lets start a new page
#sed -i 's/BENCHIT_KERNEL_CPU_LIST=".*"/BENCHIT_KERNEL_CPU_LIST="0,2,3"/g'
done <foldernames

rm -f foldernames
rm -f foldernames2
rm -f foldernames3
find ../arch_x86_64/memory_bandwidth/C/pthread/SSE2-Flush/* -maxdepth 0 -exec readlink -f {} >> foldernames2 \;
find ../arch_x86_64/memory_bandwidth/C/pthread/SSE2-No-Flush/* -maxdepth 0 -exec readlink -f {} >> foldernames3 \;

paste -d'\n' foldernames2 foldernames3 > foldernames

printf "\n \\\clearpage \section{Bandwidth}\n" >> allPlots.tex
while read p; do
	rm filenames #reset filenames
	rm temp
	find $p -name '*.bit' -exec readlink -f {} >> filenames \;
	i=0
	rm temp2
	while read bit; do
		if [ $i -eq 0 ] # If first file in folder
		then
			#Emmit Title and beginfigure
			sed -n '/kernelstring=/,/date=/p' $bit >> temp
			sed -i 's/kernelstring="arch_x86_64.memory_bandwidth.C.pthread.//g' temp
			sed -i 's/date=//g' temp
			#printf $(cat temp)
			printf "\n\subsection{" >>allPlots.tex
			printf $(cat temp) >> allPlots.tex
			printf "}\n" >> allPlots.tex
			printf "\n \\\\begin{figure}[!htbp]" >> allPlots.tex

		fi
		#printf $bit

		#emmit graphic
		printf "\includegraphics[scale = 0.7]{{" >> allPlots.tex
		printf $bit >> allPlots.tex
		printf ".gp}.eps} ">> allPlots.tex

		#build comment
		printf "Plot " >> temp2
		printf $i" :" >> temp2
		sed -n '/use mode: /,/-/{p;q;}' $bit >> temp2


		i=$((i+1))
	done <filenames
		printf "\\\\end{figure}\n" >> allPlots.tex
		sed -i 's/comment=" mov(+0), alloc: L, hugepages: 1, use//g' temp2
		sed -i 's/comment=" movdqa(+0), alloc: L, hugepages: 1, use//g' temp2
		sed -i 's/- M(2)//g' temp2
		sed -i 's/\n//g' temp2

		#emmit comment
		cat temp2 >> allPlots.tex

	printf "\clearpage" >>allPlots.tex # finished one folder lets start a new page
#sed -i 's/BENCHIT_KERNEL_CPU_LIST=".*"/BENCHIT_KERNEL_CPU_LIST="0,2,3"/g'
done <foldernames

# end
printf "\\\\end{document}" >> allPlots.tex
printf $i

#create PDF
#pdflatex -shell-escape -interaction=nonstopmode allPlots.tex

mv allPlots.pdf $(date +%F).pdf
mv $(date +%F).pdf ../pdfs/
#evince ../pdfs/$(date +%F).pdf

















#find ../arch_x86_64/ -name '*.gp' -exec readlink -f {} >> filenames \;

#i=0
#printf "\n \clearpage " >> allPlots.tex
#while read p; do

#if [ `expr $i % 2` -eq 0 ] # begin figure
#then
	#printf "\n \\\begin{figure}[tbp]" >> allPlots.tex
#fi
#printf "\includegraphics[scale = 0.6]{{" >> allPlots.tex
	#printf $p >> allPlots.tex
	#printf "}.eps}">> allPlots.tex
##\caption{not created yet}

#if [ `expr $i % 2` -eq 1 ] # close if figure contains 2 plots
#then
	#printf "\end{figure}" >> allPlots.tex
#fi

#if [ `expr $i` -eq 4 ] #clear page and close if 5 plots on page
#then
	#printf "\end{figure} \n \clearpage " >> allPlots.tex
	#i=-1 #reset
#fi

#i=$((i+1))
#done <filenames

##close final figure if necessary
#if [ `expr $i % 2` -eq 1 ]
#then
		#printf "\end{figure}" >> allPlots.tex
#fi





